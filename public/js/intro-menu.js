introMenuAnimation()

function introMenuAnimation() {
  const svgContainer = document.querySelector("svg");
const circlesTimeline = anime.timeline({
  easing: "linear",
  duration: 2500,
  loop: true
});

anime.set(["svg .circle1", "svg .circle2"], {
  rotate: function() {
    return "-90";
  }
});

circlesTimeline
  .add({
    targets: ["svg .circle1"],
    keyframes: [
      { strokeDashoffset: 1256 },
      { strokeDashoffset: 500 },
      { rotate: 270 },
      { strokeDashoffset: -1256 }
    ]
  })
  .add(
    {
      targets: ["svg .circle2"],
      keyframes: [{ strokeDashoffset: 1256 }, { strokeDashoffset: -1256 }]
    },
    0
  );

const dotsTimeline = anime
  .timeline({
    easing: "linear"
  })
  .add(
    {
      targets: "svg .dot1",
      opacity: 1,
      duration: 100
    },
    600
  )
  .add(
    {
      targets: "svg .dot2",
      opacity: 1,
      duration: 100
    },
    900
  )
  .add(
    {
      targets: "svg .dot3",
      opacity: 1,
      duration: 100
    },
    1000
  )
  .add(
    {
      targets: "svg .dot4",
      opacity: 1,
      duration: 100
    },
    1200
  )
  .add(
    {
      targets: "svg .dot5",
      opacity: 1,
      duration: 100
    },
    1300
  );
}
