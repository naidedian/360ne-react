import React from "react";
import Home from "./components/Home";
import Loader from "./components/Loader";
import NavBar from "./components/NavBar";
// import MissionVision from "./components/MissionVision";
// import OurTeam from "./components/OurTeam";
// import FinancialPlanning from "./components/FinancialPlanning";
// import OurHistory from "./components/OurHistory";
import OurCommitment from "./components/OurCommitment";
// import About from "./components/About";
// import RiskManagement from "./components/RiskManagement";
// import InvestmentManagement from "./components/InvestmentManagement";
// import InstitutionalBusinessSolutions from "./components/InstitutionalBusinessSolutions";
// import EducationalConcepts from "./components/EducationalConcepts";
// import LetsTalk from "./components/LetsTalk";
import Footer from "./components/Footer";
import OurCompany from "./components/OurCompany";
import Contact from "./components/Contact";
// import Slider from "./components/Slider";
import EducationalConcepts from "./components/EducationalConcepts";
import FallBack from "./components/404";
import "./App.css";

import {
	BrowserRouter as Router,
	Route,
	Switch,
	Redirect,
	Link
} from "react-router-dom";

// import { Element, Link } from "react-scroll";

class App extends React.Component {
	constructor(props) {
		super(props);

		this.state = {
			loading: true,
			hideNav: true
		};
	}

	componentDidMount() {
		this.wait(2000);
		this.findPath();
	}

	wait = async (milliseconds = 2000) => {
		await this.sleep(milliseconds);
		this.setState({
			loading: false
		});
	};

	sleep = (milliseconds) => {
		return new Promise((resolve) => setTimeout(resolve, milliseconds));
	};

	findPath = async () => {
		const paths = window.location.pathname.split("/");
		const locatePath = paths.indexOf("404");
		const is404 = !(locatePath > 0);

		await this.setState({
			hideNav: is404
		});
	};

	render() {
		return (
			<>
				{this.state.loading && <Loader />}
				<Router forceRefresh>
					{this.state.hideNav && <NavBar />}

					<div className="App">
						<Switch>
							<Route path="/" exact>
								{/* <Slider /> */}
								<div className="main-banner-container">
									<img
										className="img-responsive"
										src="/images/Banners/welcome_360ne.png"
										alt="Welcome 360ne"
									/>
									<Link to="/lets-talk" className="cta-btn cta-float-align">
										Let's Talk
									</Link>
								</div>
								{/* <div className="text-center mt-3 p-2 welcome container">
									<p>
										In 360ne, we are a company aimed at offering viable
										solutions to our clients thinking about their stability and
										well-being so that each of them can have a secure future.
										That is our greatest satisfaction.
									</p>
									<p className="pt-2">We are here for you!</p>
								</div> */}
								<Home />
								<Contact />
							</Route>
							<Route path="/our-company">
								<OurCompany />
							</Route>
							{/* <Element name="aboutUs_anchor">
										<About />
									</Element> */}
							<Route path="/our-commitment">
								<OurCommitment />
							</Route>
							{/* <Element name="approach_anchor">
										<Approach360 />
									</Element> */}
							<Route path="/lets-talk">
								<Contact />
							</Route>
							<Route path="/educational-concepts">
								<EducationalConcepts />
							</Route>
							<Route path="/404">
								<FallBack />
							</Route>
							<Redirect to="/404" />
						</Switch>
						<Footer />
						{/* <Router forceRefresh={true}>
            <Switch>
              <Route path="/" exact={true}>
                <Home />
              </Route>
              <Route path="/mission-vision">
                <MissionVision />
              </Route>
              <Route path="/our-team">
                <OurTeam />
              </Route>
              <Route path="/financial-planning">
                <FinancialPlanning />
              </Route>
              <Route path="/our-history">
                <OurHistory />
              </Route>
              <Route path="/our-commitment">
                <OurCommitment />
              </Route>
              <Route path="/about">
                <About />
              </Route>
              <Route path="/risk-management">
                <RiskManagement />
              </Route>
              <Route path="/investment-management">
                <InvestmentManagement />
              </Route>
              <Route path="/institutional-business-solutions">
                <InstitutionalBusinessSolutions />
              </Route>
              <Route path="/educational-concepts">
                <EducationalConcepts />
              </Route>
              <Route path="/lets-talk">
                <LetsTalk />
              </Route>
              <Route
                path="/404"
                component={() => (
                  <div className="text-center m-3">
                    <h1>
                      The page you requested does not exist. <br />
                      <span role="img" aria-label="sad">
                        &#x1F62D;
                      </span>
                    </h1>
                  </div>
                )}
              />
              <Redirect to="/404" />
            </Switch>
        </Router> */}
					</div>
					{/* <!-- /page --> */}
					<a href="/#" id="to-the-top" className="fixbtt bg-hover-theme">
						<i className="fa fa-chevron-up"></i>
					</a>
				</Router>
			</>
		);
	}
}

export default App;
