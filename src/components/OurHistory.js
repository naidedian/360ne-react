import React from "react";
import { Link } from "react-router-dom";

const OurHistory = props => {
  return (
    <div>
      <section className="section-bg-wrapper bg-history">
        <div className="sub-header-container">
          {/* <h3>Mission & Vision</h3> */}
          <ol className="breadcrumb">
            <li>
              <a href="/360ne">
                <i className="fa fa-home"></i> HOME
              </a>
            </li>
            <li>
              <Link to="#">A 360 APPROACH</Link>
            </li>
            <li className="active">OUR HISTORY</li>
          </ol>
        </div>
      </section>

      <section>
        <div className="container">
          <div className="row">
            <div className="col-md-9">
              {/* <!-- Our history --> */}
              <div className="bio-info">
                <h2>Our History</h2>
                <p>
                  April 2012. A project was born of an idea; a commitment was
                  formed by a necessity and a dream was becoming a reality. In
                  the midst of an economic recession, of a government in a
                  fiscal crisis and an investment community that lost its faith
                  in the system and the powerhouses, our firm was born. With a
                  passion for the correct and the energy to spread the news, we
                  took on this great adventure. With the right tools, a solid
                  platform and a just business model, we began helping others,
                  helping many; both individuals and institutions to manage
                  their finances the correct way. Because we have the
                  capabilities and the intent, we work with everyone to
                  accomplish their goals and to manage expectations. Like every
                  good story it must have an end. Good for us, our history
                  continues to be written, and as we play an important part of
                  the story, you on the other hand are the main character of
                  this fascinating journey.
                </p>
              </div>
              {/* <!-- <div className="main-page">
								<div className="history-timeline-h-warp">
									<div className="timeline-centered">
								        <article className="timeline-entry">
								        	<p className="h-year">1970</p>
								            <div className="timeline-entry-inner">
								                <div className="timeline-icon">
								                </div>
								                <div className="timeline-label">
								                	<h4>Establishment of the company</h4>
								                	<p>Temporibus autem quibusdam et aut officiis debitis is aut rerum necessitatibus saepes eveniet ut et seo lage voluptates repudiandae sint et molestiae non mes  for Creating  futures through building preservation. Rising commodity prices tend to push bond prices lower.</p>
								                </div>
								            </div>
								        </article>
								        <article className="timeline-entry">
								        	<p className="h-year">1985</p>
								            <div className="timeline-entry-inner">
								                <div className="timeline-icon">
								                </div>
								                <div className="timeline-label">
								                	<h4>Endless Ambition </h4>
								                	<p>Temporibus autem quibusdam et aut officiis debitis is aut rerum necessitatibus saepes eveniet ut et seo lage voluptates repudiandae sint et molestiae non mes  for Creating  futures through building preservation. Rising commodity prices tend to push bond prices lower.</p>
								                </div>
								            </div>
								        </article>
								        <article className="timeline-entry">
								        	<p className="h-year">2000</p>
								            <div className="timeline-entry-inner">
								                <div className="timeline-icon">
								                </div>
								                <div className="timeline-label">
								                	<h4>Strength of the Relationship</h4>
								                	<p>Temporibus autem quibusdam et aut officiis debitis is aut rerum necessitatibus saepes eveniet ut et seo lage voluptates repudiandae sint et molestiae non mes  for Creating  futures through building preservation. Rising commodity prices tend to push bond prices lower.</p>
								                </div>
								            </div>
								        </article>
								        <article className="timeline-entry">
								        	<p className="h-year">2012</p>
								            <div className="timeline-entry-inner">
								                <div className="timeline-icon">
								                </div>
								                <div className="timeline-label">
								                	<h4>Together We Build</h4>
								                	<p>Temporibus autem quibusdam et aut officiis debitis is aut rerum necessitatibus saepes eveniet ut et seo lage voluptates repudiandae sint et molestiae non mes  for Creating  futures through building preservation. Rising commodity prices tend to push bond prices lower.</p>
								                </div>
								            </div>
								        </article>
								        <article className="timeline-entry">
								        	<p className="h-year">2016</p>
								            <div className="timeline-entry-inner">
								                <div className="timeline-icon">
								                </div>
								                <div className="timeline-label">
								                	<h4>We’re Number One</h4>
								                	<p>Temporibus autem quibusdam et aut officiis debitis is aut rerum necessitatibus saepes eveniet ut et seo lage voluptates repudiandae sint et molestiae non mes  for Creating  futures through building preservation. Rising commodity prices tend to push bond prices lower.</p>
								                </div>
								            </div>
								        </article>
								    </div>
								</div>
							</div> --> */}
            </div>
            <div className="col-md-3">
              <div className="sideabar">
                <div className="widget widget-sidebar widget-list-link">
                  <h4 className="title-widget-sidebar">Company</h4>
                  <ul className="wd-list-link">
                    <li>
                      <Link to="/mission-vision">Mission & Vision</Link>
                    </li>
                    <li>
                      <Link to="/our-history">Our History</Link>
                    </li>
                    <li>
                      <Link to="/our-commitment">Our Commitment</Link>
                    </li>
                    <li>
                      <Link to="/our-team">Our Team</Link>
                    </li>
                    <li>
                      <Link to="/about">About Us</Link>
                    </li>
                  </ul>
                </div>
                {/* <!-- <div className="widget widget-sidebar widget-text-block">
									<h4 className="title-widget-sidebar">
										Company in Lines
									</h4>
									<div className="wd-text-warp">
										<p>Temporibus autem quibusdam et aut officiis debitis is  necessitatibus saepes eveniet ut et seo repudiandae sint et molestiae non Creating futures seon through world.</p>
										<a href="#" className="ot-btn btn-main-color" >
											<i className="fa fa-download" aria-hidden="true"></i>
											Download Presentation</a>
									</div>
								</div> --> */}
                {/* <!-- <div className="widget-sidebar widget widget-html">
									<div className="wd-html-block">
										<a href="#">
											<img src="http://placehold.it/269x180/ccc.jpg" className="img-responsive" alt="Image">
										</a>
										<div className="content-wd-html-inner">
											<span>HIRING</span>
											<p>
												COME TO JOIN OUR TEAM !
											</p>
										</div>
										<a href="#" className="ot-btn btn-sub-color" >
											Join Now
										</a>
									</div>
								</div> --> */}
              </div>
            </div>
          </div>
        </div>
      </section>
    </div>
  );
};

export default OurHistory;
