import React from "react";
import { Link } from "react-router-dom";

const Footer = (props) => {
	return (
		<div>
			{/* <!-- Footer --> */}
			<footer className="overlay">
				<div className="container">
					<div className="row">
						{/* <div className="col-md-3 col-sm-6">
              <div className="widget widget-footer widget-footer-text">
                <div className="title-block title-on-dark title-xs">
                  <h4>About 360 Rodríguez Group</h4>
                  <span className="bottom-title"></span>
                </div>
                <p>
                  April 2012. A project was born of an idea; a commitment was
                  formed by a necessity and a dream was becoming a reality. In
                  the midst of an economic recession, of a government in a
                  fiscal crisis and an investment community that lost its faith
                  in the system and the powerhouses, our firm was born. With a
                  passion for the correct and the energy to spread the news, we
                  took on this great adventure.
                </p>
                <ul className="widget widget-footer widget-footer-social-1">
                  <li>
                    <a
                      href="https://facebook.com/360rodriguezgroup/"
                      target="_blank"
                      rel="noopener noreferrer"
                    >
                      <i className="fa fa-facebook" aria-hidden="true"></i>
                    </a>
                  </li>
                  <li>
                    <a
                      href="https://twitter.com"
                      target="_blank"
                      rel="noopener noreferrer"
                    >
                      <i className="fa fa-twitter" aria-hidden="true"></i>
                    </a>
                  </li>
                </ul>
              </div>
            </div> */}
						<div className="col-md-4 col-sm-6">
							<div className="widget widget-footer widget-footer-img">
								<div className="title-block title-on-dark title-xs">
									<h4>Our Branches</h4>
									<span className="bottom-title"></span>
								</div>
								<Link to="/">
									<img
										src="/images/logo_white.png"
										width="200"
										className="img-responsive"
										alt="360 Rodríguez Group Logo"
									/>
								</Link>
							</div>
						</div>
						<div className="col-md-4 col-sm-6">
							<div className="widget widget-footer widget-footer-icon-link">
								<div className="title-block title-on-dark title-xs">
									<h4>Head Office</h4>
									<span className="bottom-title"></span>
								</div>

								<ul className="icon-link-list-icon">
									<li>
										<i className="fa fa-map-marker" aria-hidden="true"></i>
										4745 Ave. Isla Verde, Villas del Mar Este, CM 3, Carolina,
										PR 00979
									</li>
									<li>
										<i className="fa fa-envelope-o" aria-hidden="true"></i>{" "}
										<a href="mailto:info@360rodriguezgroup.com">
											info@360rodriguezgroup.com
										</a>
									</li>
									<li>
										<i className="fa fa-phone" aria-hidden="true"></i>
										<a href="tel:787-200-9124 ">787-200-9124 </a>
									</li>
								</ul>
							</div>
						</div>
						<div className="col-md-4 col-sm-6">
							<div className="widget widget-footer widget-footer-list-link pl-4">
								<div className="title-block title-on-dark title-xs">
									<h4>Menu</h4>
									<span className="bottom-title"></span>
								</div>
								<ul>
									<li>
										<Link to="/">Home</Link>
									</li>
									<li>
										<Link to="/about-us">About Us</Link>
									</li>
									<li>
										<Link to="/services">Services</Link>
									</li>
									<li>
										<Link to="/blog">Blog</Link>
									</li>
									<li>
										<Link to="/lets-talk">Contact</Link>
									</li>
								</ul>
							</div>
						</div>
					</div>
					<div className="copyright-warp">
						<div className="copyright-text">
							<p className="text-white">
								{new Date().getFullYear()} &copy;{" "}
								<Link to="/" className="text-white">
									360ne
								</Link>
							</p>
						</div>
					</div>
				</div>
			</footer>
			{/* <!-- /Footer --> */}

			{/* <section className="no-padding cr-h1">
				<div className="container">
					<div className="row">
						<div className="col-md-12">
							<div className="copyright-warp cr-1 p-1 flex-row">
								<div className="copyright-list-link">
									<ul>
										<li>
											<Link
												to="mainHome_anchor"
												spy={true}
												smooth={true}
												duration={1000}
												offset={-100}
											>
												Mission & Vision
											</Link>
										</li>
										<li>
											<Link
												to="ourCompany_anchor"
												spy={true}
												smooth={true}
												duration={1000}
												offset={-100}
											>
												Our Company
											</Link>
										</li>
										<li>
											<Link
												to="contact_anchor"
												spy={true}
												smooth={true}
												duration={1000}
												offset={-100}
											>
												Let's Talk
											</Link>
										</li>
									</ul>
								</div>
							</div>
						</div>
					</div>
				</div>
			</section> */}
			{/* <!-- /copyright --> */}
		</div>
	);
};

export default Footer;
