import React from "react";
// import { Tab, TabTitle, TabContent, TabPane } from "./Tab/index";
// import Team from "./OurTeam";

// Images
import creditUnionImg from "../images/icons/credit-unions.png";
import endowmentProfessionalImg from "../images/icons/endowment-ad-proffesional.png";
import accessInformationImg from "../images/icons/access-information.png";
import nonProfitImg from "../images/icons/non-profit.png";
import roadMapImg from "../images/icons/road-map-sucesess.png";
import strongImg from "../images/icons/strong.png";

const Home = (props) => {
	return (
		<div>
			{/* <!-- royal_loader --> */}
			<div id="home">
				<div className="container">
					<div className="row mt-2">
						<div className="col-md-6 p-2">
							<h3>Mission</h3>
							<p>
								Provide our clients with a financial planning experience that is
								tailored to their needs and goals. We evaluate, recommend and
								implement financial strategies that mitigate potential risks,
								maximizes financial opportunities and creates lasting legacies.
							</p>
						</div>
						<div className="col-md-6 p-2">
							<h3>Vision</h3>
							<p>
								To be leaders and pioneers in the investment community of Puerto
								Rico and Tampa and to be recognized as the primary financial
								provider of the clients we serve. We strive for excellence and
								we have the vision to continue to be of impact to the following
								industries:
							</p>
							<ul className="ml-3">
								<li>Credit Union</li>
								<li>Nonprofit institutions</li>
								<li>Medical Groups</li>
								<li>Endowments and foundations</li>
							</ul>
						</div>
					</div>
					<div className="row mt-3">
						<div className="col-md-4 mb-2">
							<img
								src={accessInformationImg}
								className="img-responsive round center-block"
								alt="360NE Strong Compliance"
								width="200"
							/>
							<h4 className="text-center mt-1">Strong Compliance</h4>
						</div>
						<div className="col-md-4 mb-2">
							<img
								src={roadMapImg}
								className="img-responsive round center-block"
								alt="360NE Road Map to Success"
								width="200"
							/>
							<h4 className="text-center mt-1">Road Map to Success</h4>
						</div>
						<div className="col-md-4 mb-2">
							<img
								src={strongImg}
								className="img-responsive round center-block"
								alt="360NE Constant Access to Your Information"
								width="200"
							/>
							<h4 className="text-center mt-1">
								Constant Access to Your Information
							</h4>
						</div>
					</div>
					<div className="row mt-3">
						<div className="title-block text-center title-pd">
							<span className="top-title "></span>
							<h2>Industries</h2>
							<span className="bottom-title"></span>
						</div>
						<div className="col-md-4 mb-2">
							<img
								src={creditUnionImg}
								className="img-responsive center-block"
								alt="360NE Credit Unions"
							/>
							<h4 className="text-center mt-1">Credit Unions</h4>
						</div>
						<div className="col-md-4 mb-2">
							<img
								src={nonProfitImg}
								className="img-responsive center-block"
								alt="360NE Non Profit"
							/>
							<h4 className="text-center mt-1">Non Profit</h4>
						</div>
						<div className="col-md-4 mb-2">
							<img
								src={endowmentProfessionalImg}
								className="img-responsive center-block"
								alt="360NE Endowment Ad Professional"
							/>
							<h4 className="text-center mt-1">Endowment Ad Professional</h4>
						</div>
					</div>
				</div>
				{/* <section className="section-bg-wrapper bg-value-proposition"></section>
				<Tab className="">
					<TabTitle activeClass="active" link="mission">
						MISSION
					</TabTitle>
					<TabTitle link="vision">VISION</TabTitle>
					<TabTitle link="proposition">VALUE PROPOSITION</TabTitle>
					<TabTitle link="ourstory">OUR STORY</TabTitle>
					<TabTitle link="ourteam">OUR TEAM</TabTitle>
				</Tab>
				<TabContent className="width-1024 center-block">
					<TabPane id="mission" activePane="active">
						<div>
							<h3>Mission</h3>
							<p>
								Provide our clients with a financial planning experience that is
								tailored to their needs and goals. We evaluate, recommend and
								implement financial strategies that mitigate potential risks,
								maximizes financial opportunities and creates lasting legacies.
							</p>
						</div>
					</TabPane>
					<TabPane id="vision">
						<h3>Vision</h3>
						<p>
							To be leaders and pioneers in the investment community of Puerto
							Rico and Tampa and to be recognized as the primary financial
							provider of the clients we serve. We strive for excellence and we
							have the vision to continue to be of impact to the following
							industries:
						</p>
						<ul>
							<li>Credit Union</li>
							<li>Nonprofit institutions</li>
							<li>Medical Groups</li>
							<li>Endowments and foundations</li>
						</ul>
					</TabPane>
					{/* <TabPane id="institutions">
            <h3>Institutions</h3>
            <p>
              To collaborate as financial advisors in the development and
              continuous revision of the institution’s investment policy, to be
              an extension of the institution’s investment committee and to
              provide relevant and optimal investment alternatives in our
              capacity as investment managers.
            </p>
          </TabPane> 
					<TabPane id="proposition">
						<h3>Value Proposition</h3>
						<p>Value proposition</p>
					</TabPane>
					<TabPane id="ourstory">
						<p>
							April 2012. A project was born of an idea; a commitment was formed
							by a necessity and a dream was becoming a reality. In the midst of
							an economic recession, of a government in a fiscal crisis and an
							investment community that lost its faith in the system and the
							powerhouses, our firm was born. With a passion for the correct and
							the energy to spread the news, we took on this great adventure.
							With the right tools, a solid platform and a just business model,
							we began helping others, helping many; both individuals and
							institutions to manage their finances the correct way.
						</p>
						<p>
							Because we have the capabilities and the intent, we work with
							everyone to accomplish their goals and to manage expectations.
							Like every good story it must have an end. Good for us, our
							history continues to be written, and as we play an important part
							of the story, you on the other hand are the main character of this
							fascinating journey.
						</p>
					</TabPane>
					<TabPane id="ourteam">
						<Team />
					</TabPane>
				</TabContent> */}
			</div>
		</div>
	);
};

export default Home;
