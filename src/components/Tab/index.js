import React from "react";

export const Tab = ({ children, className }) => {
  return (
    <ul className={`nav nav-tabs text-center ${className}`}>{children}</ul>
  );
};

export const TabTitle = ({ children, activeClass, link }) => (
  <>
    <li className={activeClass}>
      <a href={`#${link}`} data-toggle="tab">
        {children}
      </a>
    </li>
  </>
);

export const TabContent = ({ children, className }) => (
  <div className={`tab-content ${className}`}>{children}</div>
);

export const TabPane = ({ children, id, activePane = "" }) => (
  <div id={id} className={`tab-pane fade in ${activePane}`}>
    {children}
  </div>
);
