import React from "react";
import Approach360 from "./Approach";

const OurCompany = (props) => (
	<>
		<div className="section-separation">
			<section className="section-bg-wrapper bg-different"></section>
			<div className="width-1024 center-block text-center">
				<h2 className="text-center mt-2">We Are Your Trust!</h2>
				<p className="par-med p-1">
					Having someone there you trust to help ensure you're making all the
					right financial moves can be invaluable. We have now have the freedom
					and flexibility to provide a truly experience and tailor our services
					and investment products to best suit the needs of our clients. Being a
					CERTIFIED FINANCIAL PLANNER™ we have required to honor our Fiduciary
					duty above all. We are an honest company with a great professional
					team ready to offer ideal tools for the benefit of all customers.
				</p>
			</div>
		</div>
		<Approach360 />
		{/* <Tab>
      <TabTitle link="overview" activeClass="active">
        OVERVIEW
      </TabTitle>
      <TabTitle link="advantages">ADVANTAGES</TabTitle>
    </Tab>
    <TabContent className="width-1024 center-block">
      <TabPane id="overview" activePane="active">
        <Overview />
      </TabPane>
      <TabPane id="advantages">
        <Advantages />
      </TabPane>
    </TabContent> */}

		{/*<section id="about" className="about-h1">
			<div className="container-fluid ml-3">
				<div className="row">
					<div className="col-md-12">
						<div className="demo-about-h1">
							<div className="">
								<div className="left-about-h1">
									<div className="title-block text-center">
										<span className="top-title"></span>
										<h2>Our Company</h2>
										<p className="sub-title">As always, partners grows with you!</p>
										<span className="bottom-title"></span>
									</div>
									
										<Link to="educational-concepts" className="d-inline-block iconbox-link">
											<div className="text-center">
											<img src="/images/Home/icon_educational_concepts.png" alt="Educational Concepts" className="link-home-icon"/>
											</div>
										</Link>
										<Link to="newsletter" className="d-inline-block iconbox-link">
											<div className="text-center">
											<img src="/images/Home/icon_newsletter.png" alt="Newsletter" className="link-home-icon"/>
											</div>
										</Link>
									</div>
								</div>
							</div>
							<div className="col-md-6">
									<div className="right-about-h1">
										<img src="http://placehold.it/923x759/ccc.jpg" className="img-responsive" alt="Image"/>
									</div>
  								</div>
						</div>
					</div>
				</div>
</section>*/}
	</>
);

export default OurCompany;
