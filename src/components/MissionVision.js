import React from "react";
import { Link } from "react-router-dom";

const MissionVision = props => {
  return (
    <div>
      <section className="section-bg-wrapper bg-mission">
        <div className="sub-header-container">
          {/* <h3>Mission & Vision</h3> */}
          <ol className="breadcrumb">
            <li>
              <a href="/360ne">
                <i className="fa fa-home"></i> HOME
              </a>
            </li>
            <li>
              <Link to="#">A 360 APPROACH</Link>
            </li>
            <li className="active">Mission, Vision & Institutions</li>
          </ol>
        </div>
      </section>

      <section className="no-padding">
        <div className="container">
          <div className="row">
            <div className="whyus-warp-h2 whyus-about">
              <div className="col-md-6">
                <div className="left-whyus-h2">
                  <div className="demo-style-1-warp">
                    <img
                      src="/images/mission-vision-vbanner.jpg"
                      className="img-responsive"
                      alt="Mission & Vision"
                    />
                    {/* <div className="demo-style-1-box-text right">
                      <p>Today,</p>
                      <p>Tomorrow,</p>
                      <p>Together.</p>
                    </div> */}
                  </div>
                </div>
              </div>
              <div className="col-md-6">
                <div className="right-whyus-h2">
                  <div className="iconbox-warp ">
                    <h3 className="title-mix">
                      <strong>MISSION</strong>, <strong>VISION</strong> &
                      <strong> INSTITUTIONS</strong>
                    </h3>
                    <div>
                      <h4>Mission</h4>
                      <p>
                        Provide our clients with a financial planning experience
                        that is tailored to their particular needs and goals. We
                        evaluate, recommend and implement financial strategies
                        that mitigate potential risks, maximizes financial
                        opportunities and creates a lasting legacies.
                      </p>
                    </div>
                    <div>
                      <h4>Institutions</h4>
                      <p>
                        To collaborate as financial advisors in the development
                        and continuous revision of the institution’s investment
                        policy, to be an extension of the institution’s
                        investment committee and to provide relevant and optimal
                        investment alternatives in our capacity as investment
                        managers.
                      </p>
                    </div>
                    <div>
                      <h4>Vision</h4>
                      <p>
                        To be leaders and pioneers in the investment community
                        of Puerto Rico and Tampa and to be recognized as the
                        primary financial provider of the clients we serve. We
                        strive for excellence and we have the vision to continue
                        to be of impact to the following industries:
                        <ul className="text-left ml-4">
                          <li>Credit Union</li>
                          <li>Nonprofit institutions</li>
                          <li>Medical Groups</li>
                          <li>Endowments and foundations</li>
                        </ul>
                      </p>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </section>
      {/* <!-- about description --> */}

      {/* <!-- <section className="no-padding bg-counter-h1">
				<div className="container">
					<div className="row">
						<div className="warp-counter">
							<div className="col-md-3 col-sm-6">
								<div className="counter-inline">
									<span className="icon icon-stats-bars"></span>
									<span className="counter">460</span>
									<p>Cases completed</p>
								</div>
							</div>
							<div className="col-md-3 col-sm-6">
								<div className="counter-inline">
									<span className="icon icon-users"></span>
									<span className="counter">29</span>
									<p>Consultants</p>
								</div>
							</div>
							<div className="col-md-3 col-sm-6">
								<div className="counter-inline">
									<span className="icon icon-trophy"></span>
									<span className="counter">18</span>
									<p>Awards winning</p>
								</div>
							</div>
							<div className="col-md-3 col-sm-6">
								<div className="counter-inline">
									<span className="icon icon-library"></span>
									<span className="counter">50</span>
									<p>Years of Experiences</p>
								</div>
							</div>
						</div>
					</div>
				</div>
			</section> --> */}
      {/* <!-- couterup --> */}

      {/* <!-- <section>
				<div className="container">
					<div className="row">
						<div className="col-md-12">
							<div className="strategy-warp-h4 whyus-about">
								<div className="title-block text-center">
									<span className="top-title"></span>
									<h2>Why to choose us</h2>
									<p className="sub-title">As always, partners grows with you!</p>
									<span className="bottom-title"></span>
								</div>
								<div className="about-icon-warp">
									<div className="col-md-3">
										<div className="iconbox text-center">
											<span className="icon icon-trophy"></span>
											<h4>Professional</h4>
											<p>Lorem ipsum dolor sit , consectet adipisi elit, sed do eiusmod tempor for enim adesg ens minim veniam.</p>
										</div>
										<div className="iconbox text-center no-mgb">
											<span className="icon icon-library"></span>
											<h4>Vibrant</h4>
											<p>Lorem ipsum dolor sit , consectet adipisi elit, sed do eiusmod tempor for enim adesg ens minim veniam.</p>
										</div>
										
									</div>
									<div className="col-md-3">
										<div className="iconbox text-center">
											<span className="icon icon-profile"></span>
											<h4>Personality</h4>
											<p>Lorem ipsum dolor sit , consectet adipisi elit, sed do eiusmod tempor for enim adesg ens minim veniam.</p>
										</div>
										<div className="iconbox text-center no-mgb">
											<span className="icon icon-briefcase"></span>
											<h4>Experienced</h4>
											<p>Lorem ipsum dolor sit , consectet adipisi elit, sed do eiusmod tempor for enim adesg ens minim veniam.</p>
										</div>

									</div>
									<div className="col-md-3">
										<div className="iconbox text-center">
											<span className="icon icon-power"></span>
											<h4>Energy</h4>
											<p>Lorem ipsum dolor sit , consectet adipisi elit, sed do eiusmod tempor for enim adesg ens minim veniam.</p>
										</div>
										<div className="iconbox text-center no-mbg">
											<span className="icon icon-key"></span>
											<h4>Honesty</h4>
											<p>Lorem ipsum dolor sit , consectet adipisi elit, sed do eiusmod tempor for enim adesg ens minim veniam.</p>
										</div>
									</div>

									<div className="col-md-3">
										<div className="iconbox text-center">
											<span className="icon icon-rocket"></span>
											<h4>Quanlity</h4>
											<p>Lorem ipsum dolor sit , consectet adipisi elit, sed do eiusmod tempor for enim adesg ens minim veniam.</p>
										</div>
										<div className="iconbox text-center no-mgb">
											<span className="icon icon-user-check"></span>
											<h4>Trust</h4>
											<p>Lorem ipsum dolor sit , consectet adipisi elit, sed do eiusmod tempor for enim adesg ens minim veniam.</p>
										</div>
									</div>
								</div>

							</div>
						</div>
					</div>
				</div>
			</section> --> */}
      {/* <!-- /why us --> */}

      {/* <!-- <section className="bg-light-grey">
				<div className="container">
					<div className="row">
						<div className="col-md-12">
							<div id="partner-h2" className="owl-partner-h2">
								<div className="item-partner" >
									<a href="#"><img src="http://placehold.it/269x160/ccc.jpg" className="img-responsive partner-img" alt="Image"></a>							
								</div>
								<div className="item-partner" >
									<a href="#"><img src="http://placehold.it/269x160/ccc.jpg" className="img-responsive partner-img" alt="Image"></a>							
								</div>
								<div className="item-partner" >
									<a href="#"><img src="http://placehold.it/269x160/ccc.jpg" className="img-responsive partner-img" alt="Image"></a>							
								</div>
								<div className="item-partner" >
									<a href="#"><img src="http://placehold.it/269x160/ccc.jpg" className="img-responsive partner-img" alt="Image"></a>							
								</div>
								<div className="item-partner" >
									<a href="#"><img src="http://placehold.it/269x160/ccc.jpg" className="img-responsive partner-img" alt="Image"></a>							
								</div>
								<div className="item-partner" >
									<a href="#"><img src="http://placehold.it/269x160/ccc.jpg" className="img-responsive partner-img" alt="Image"></a>							
								</div>
								<div className="item-partner" >
									<a href="#"><img src="http://placehold.it/269x160/ccc.jpg" className="img-responsive partner-img" alt="Image"></a>							
								</div>
								<div className="item-partner" >
									<a href="#"><img src="http://placehold.it/269x160/ccc.jpg" className="img-responsive partner-img" alt="Image"></a>							
								</div>
								<div className="item-partner" >
									<a href="#"><img src="http://placehold.it/269x160/ccc.jpg" className="img-responsive partner-img" alt="Image"></a>							
								</div>
								<div className="item-partner" >
									<a href="#"><img src="http://placehold.it/269x160/ccc.jpg" className="img-responsive partner-img" alt="Image"></a>							
								</div>

							</div>
						</div>
					</div>
				</div>
			</section> --> */}
      {/* <!-- /Partner --> */}

      {/* <!-- <section className="bg-subcr-1">
				<div className="container">
					<div className="row">
						<div className="col-md-12">
							<div className="subcribe-warp">
								<p className="sub-text-subcri">Newsletter for recieve </p>
								<form className="form-inline form-subcri">
								  <div className="form-group">
								    <label for="exampleInputName2"><small>our <span> latest company</span>updates</small></label>
								    <input type="text" className="form-control" id="exampleInputName2" placeholder="Your E-mail Address">
								  </div>
								  <button type="submit" className="btn-subcrib"><i className="fa fa-paper-plane" aria-hidden="true"></i></button>
								</form>
							</div>
						</div>
					</div>
				</div>
			</section> --> */}
      {/* <!-- /Subcribe --> */}
    </div>
  );
};

export default MissionVision;
