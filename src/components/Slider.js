import React from "react"
import {Link} from "react-router-dom"

const Slider = props => (
		<section id="slider" className="no-padding">
        <div id="slide_1_wrapper" className="rev_slider_wrapper fullwidthbanner-container">
            {/* <!-- START REVOLUTION SLIDER 5.0.7 auto mode --> */}
            <div id="slider_1" className="rev_slider fullwidthabanner slider-home1" style={{display:"none"}}
                data-version="5.0.7">
                <ul>
                    {/* <!-- SLIDE  --> */}
                    <li data-title="FNANCIAL PLANNING">
                        {/* <!-- MAIN IMAGE --> */}
                        <img src="/images/Slider/banners/financial_planning.png" alt="" data-bgposition="center top"
                            data-bgfit="cover" data-bgrepeat="no-repeat" className="img-responsive rev-slidebg" data-no-retina/>
                        <div className="tp-caption" data-x="center" data-hoffset="70" data-y="bottom" data-voffset="147"
                            data-transform_idle="o:1;" data-transform_in="x:50px;opacity:0;s:1000;e:Power3.easeOut;"
                            data-transform_out="x:50px;opacity:0;s:1000;e:Power3.easeInOut;s:1000;e:Power3.easeInOut;"
                            data-start="2000"><Link to="financial-planning" className="sl-btn-2">Ver Más</Link>
                        </div>
                    </li>
                    {/* <!-- SLIDE  --> */}
                    <li data-title="INVESTMENT MANAGEMENT">
                        {/* <!-- MAIN IMAGE --> */}
                        <img src="/images/Slider/banners/investment_management.png" alt="" data-bgposition="center top"
                            data-bgfit="cover" data-bgrepeat="no-repeat" className="rev-slidebg" data-no-retina/>
                        {/* <!-- LAYERS --> */}
                        {/* <!-- LAYER NR. 5 --> */}
                        <div className="tp-caption" data-x="center" data-hoffset="70" data-y="bottom" data-voffset="147"
                            data-transform_idle="o:1;" data-transform_in="x:50px;opacity:0;s:1000;e:Power3.easeOut;"
                            data-transform_out="x:50px;opacity:0;s:1000;e:Power3.easeInOut;s:1000;e:Power3.easeInOut;"
                            data-start="2000"><Link to="investment-management" className="sl-btn-2">Ver Más</Link>
                        </div>
                    </li>
                    {/* <!-- SLIDE  --> */}
                    <li data-title="OUR TEAM">
                        {/* <!-- MAIN IMAGE --> */}
                        <img src="/images/Slider/banners/our_team.png" alt="" data-bgposition="center top"
                            data-bgfit="cover" data-bgrepeat="no-repeat" className="rev-slidebg" data-no-retina/>
                        {/* <!-- LAYERS --> */}
                        
                        <div className="tp-caption" data-x="center" data-hoffset="70" data-y="bottom" data-voffset="147"
                            data-transform_idle="o:1;" data-transform_in="x:50px;opacity:0;s:1000;e:Power3.easeOut;"
                            data-transform_out="x:50px;opacity:0;s:1000;e:Power3.easeInOut;s:1000;e:Power3.easeInOut;"
                            data-start="2000"><Link to="our-team" className="sl-btn-2">Ver Más</Link>
                        </div>
                    </li>
                    <li data-title="RETIREMENT">
                    <img src="/images/Slider/banners/retirement.png" alt="" data-bgposition="center top"
                            data-bgfit="cover" data-bgrepeat="no-repeat" className="rev-slidebg" data-no-retina/>
                            <div className="tp-caption" data-x="center" data-hoffset="70" data-y="bottom" data-voffset="147"
                            data-transform_idle="o:1;" data-transform_in="x:50px;opacity:0;s:1000;e:Power3.easeOut;"
                            data-transform_out="x:50px;opacity:0;s:1000;e:Power3.easeInOut;s:1000;e:Power3.easeInOut;"
                            data-start="2000"><Link to="retirement" className="sl-btn-2">Ver Más</Link>
                        </div>
                    </li>
                    <li data-title="LET'S TALK">
                    <img src="/images/Slider/banners/lets_talk.png" alt="" data-bgposition="center top"
                            data-bgfit="cover" data-bgrepeat="no-repeat" className="rev-slidebg" data-no-retina/>
                            <div className="tp-caption" data-x="center" data-hoffset="70" data-y="bottom" data-voffset="147"
                            data-transform_idle="o:1;" data-transform_in="x:50px;opacity:0;s:1000;e:Power3.easeOut;"
                            data-transform_out="x:50px;opacity:0;s:1000;e:Power3.easeInOut;s:1000;e:Power3.easeInOut;"
                            data-start="2000"><Link to="lets-talk" className="sl-btn-2">Ver Más</Link>
                        </div>
                    </li>
                </ul>
            </div>
        </div>
    </section>
)

export default Slider