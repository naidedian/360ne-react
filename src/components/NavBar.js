import React, { useState, useEffect, useRef } from "react";
import { Link } from "react-router-dom";
// import { Link as Link } from "react-scroll";

const NavBar = (props) => {
	const [stickyClass, setStickyClass] = useState("");
	const navContainer = useRef(null);

	useEffect(() => {
		if (!navContainer) return;
		window.addEventListener("scroll", applySticky);
	}, []);

	const applySticky = () => {
		const navHeight = navContainer.current.clientHeight;
		const scrollPosition = Math.floor(window.scrollY);
		if (scrollPosition >= navHeight) {
			setStickyClass("sticky-nav");
		} else {
			setStickyClass("");
		}
	};
	return (
		<div>
			<nav id="menu">
				<ul>
					<li>
						<Link to="/">HOME</Link>
					</li>
					{/* <li>
						<Link
							to="aboutUs_anchor"
							spy={true}
							smooth={true}
							duration={1000}
							offset={-100}
						>
							OUR FIRM
						</Link>
					</li> */}
					<li className="dropdown">
						<Link
							to="#"
							className="dropdown-toggle"
							data-toggle="dropdown"
							aria-haspopup="true"
							aria-expanded="true"
						>
							ABOUT US
						</Link>
						<ul>
							<li>
								<Link to="#">Values</Link>
							</li>
							<li>
								<Link to="#">Team</Link>
							</li>
							<li>
								<Link to="#">CIMA</Link>
							</li>
							<li>
								<Link to="#">CFP</Link>
							</li>
							<li>
								<Link to="#">Who We Are</Link>
							</li>
						</ul>
					</li>
					<li>
						<Link to="/services">SERVICES</Link>
					</li>
					{/* <li>
            <Link
              to="approach_anchor"
              spy={true}
              smooth={true}
              duration={1000}
              offset={-100}
            >
              360 APPROACH
            </Link>
          </li> */}
					<li>
						<Link to="/blog">BLOG</Link>
					</li>
					<li>
						<Link to="/lets-talk">CONTACT</Link>
					</li>
				</ul>
			</nav>
			{/* <!-- /Mobile Menu --> */}
			<header className={`header-1-put ${stickyClass}`} ref={navContainer}>
				<div className="hover-here">
					<h4>Show Menu</h4>
				</div>
				<div className="topbar bg-blue">
					<div className="container">
						<div className="row">
							<div className="col-md-12">
								<div className="topbar-home1">
									<div className="tb-contact tb-oneline">
										<ul>
											{/* <li>
												<Link
													to="contact_anchor"
													spy={true}
													smooth={true}
													duration={1000}
													offset={-100}
												>
													<i
														className="fa fa-map-marker"
														aria-hidden="true"
													></i>
													4745 Ave. Isla Verde, Villas del Mar Este, CM 3,
													Carolina, PR 00979
												</Link>
											</li> */}
											<li>
												<a href="mailto:info@360rodriguezgroup.com">
													<i className="fa fa-envelope" aria-hidden="true"></i>{" "}
													info@360rodriguezgroup.com
												</a>
											</li>
											<li>
												<a href="tel:787-200-9124 ">
													<i className="fa fa-phone" aria-hidden="true"></i>
													787-200-9124
												</a>
											</li>
										</ul>
									</div>
									<div className="tb-social-lan language">
										<ul>
											<li>
												<a
													href="https://facebook.com/360rodriguezgroup/"
													target="_blank"
													rel="noopener noreferrer"
													data-toggle="tooltip"
													data-placement="bottom"
													title="facebook"
												>
													<i className="fa fa-facebook" aria-hidden="true"></i>
												</a>
											</li>
											<li>
												<a
													href="https://twitter.com"
													data-toggle="tooltip"
													data-placement="bottom"
													title="twitter"
												>
													<i className="fa fa-twitter" aria-hidden="true"></i>
												</a>
											</li>
											<li>
												<a
													href="https://linkedin.com"
													data-toggle="tooltip"
													data-placement="bottom"
													title="linkedin"
												>
													<i className="fa fa-linkedin" aria-hidden="true"></i>
												</a>
											</li>
										</ul>
										{/* <!-- <select className="lang">
										<option data-className="usa">English</option>
										<option data-className="italy">Italian</option>
										<option data-className="fr">French</option>
										<option data-className="gm">German</option>
									</select> --> */}
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
				{/* <!-- /topbar --> */}
				<div className="nav-warp nav-warp-h2 sticky-nav_item">
					<div className="container">
						<div className="row">
							<div className="col-md-12">
								<div className="navi-warp-home-2">
									<Link to="/" className="logo">
										<img
											src="./images/360ne_logo_transparent.png"
											className="img-responsive"
											alt="360ne Logo"
											width="150"
										/>
									</Link>
									<a href="#menu" className="btn-menu-mobile">
										<i className="fa fa-bars" aria-hidden="true"></i>
									</a>
									{/* <!-- <ul className="subnavi">
									<li><a href="#"><i className="fa fa-shopping-cart" aria-hidden="true"></i></a></li>
									<li>
										<a className="btn-search-navi" href="#/"><i className="fa fa-search" aria-hidden="true"></i></a>
										<div className="search-popup">
											<form className="form-search-navi">
												<div className="input-group">
													<input className="form-control" placeholder="Search Here" type="text">
												</div>
												<!-- /input-group 
											</form>
										</div>
									</li>
								</ul> --> */}
									<nav>
										<ul className="navi-level-1 active-subcolor nav-menu">
											<li>
												<Link to="/">HOME</Link>
											</li>
											<li className="dropdown">
												<Link
													to="#"
													className="dropdown-toggle mr-1"
													data-toggle="dropdown"
													aria-haspopup="true"
													aria-expanded="true"
												>
													ABOUT US
													<span className="caret"></span>
												</Link>
												<ul className="dropdown-menu">
													<li style={{ display: "block" }}>
														<Link to="#">Values</Link>
													</li>
													<li style={{ display: "block" }}>
														<Link to="#">Team</Link>
													</li>
													<li style={{ display: "block" }}>
														<Link to="#">CIMA</Link>
													</li>
													<li style={{ display: "block" }}>
														<Link to="#">CFP</Link>
													</li>
													<li style={{ display: "block" }}>
														<Link to="#">Who We Are</Link>
													</li>
												</ul>
											</li>
											<li>
												<Link to="/services">SERVICES</Link>
											</li>
											{/* <li>
												<Link
													to="approach_anchor"
													spy={true}
													smooth={true}
													duration={1000}
													offset={-100}
												>
													360 APPROACH
												</Link>
											</li> */}
											<li>
												<Link to="/blog">BLOG</Link>
											</li>
											<li>
												<Link to="/lets-talk">CONTACT</Link>
											</li>
										</ul>
									</nav>
								</div>
							</div>
						</div>
					</div>
				</div>
				{/* <!-- /nav --> */}
			</header>
		</div>
	);
};

export default NavBar;
