import React from "react";
import { Tab, TabTitle, TabContent, TabPane } from "./Tab/index";

const OurCommitment = props => {
  return (
    <div id="ourcommitment" className="section-separation">
      <section className="section-bg-wrapper bg-commitment"></section>
      <Tab>
        <TabTitle link="independence" activeClass="active">
          INDEPENDENCE
        </TabTitle>
        <TabTitle link="objectivity">OBJECTIVITY</TabTitle>
        <TabTitle link="competence">COMPETENCE</TabTitle>
      </Tab>
      <TabContent className="mt-3 width-1024 center-block">
        <TabPane id="independence" activePane="active">
          <div className="item-history-post">
            <p className="title-history">
              <span>INDEPENDENCE</span>
            </p>
            <p className="item-history-content">
              Our alliances are based on the needs of our clients and we have
              established these as independent contracts. This means that we
              have a duty to review each and every entity that we do business
              with, in order to evaluate the performance and level of service
              provided.
            </p>
          </div>
        </TabPane>
        <TabPane id="objectivity">
          <div className="item-history-post">
            <p className="title-history">
              <span>OBJECTIVITY</span>
            </p>
            <ol>
              <li>
                <p className="item-history-content">
                  As a CERTIFIED FINANCIAL PLANNER&#8482;, Jeffrey Rodriguez
                  owes a fiduciary duty to his clients. As a result, every
                  action and recommendation that we make is geared towards
                  achieving the financial goals and providing for the financial
                  needs of the clients.
                </p>
              </li>
              <li>
                <p className="item-history-content">
                  As an independent firm we have no proprietary products and no
                  corporate agenda. This provides reassurance to our clients
                  that our only commitment is their financial success.
                </p>
              </li>
            </ol>
          </div>
        </TabPane>
        <TabPane id="competence">
          <div className="item-history-post">
            <p className="title-history">
              <span>COMPETENCE</span>
            </p>
            <ol>
              <li>
                <p className="item-history-content">
                  360 Rodriguez Group, LLC was formed to operate as a boutique
                  financial services company. Our vision “to be THE primary
                  financial advisors of our clients”, requires us to maintain a
                  level of competence above and beyond our competitors.
                </p>
              </li>
              <li>
                <p>Jeffrey Rodriguez, CFP&reg;</p>
                <ul>
                  <li className="ml-1 mb-1">Investment Portfolio Management</li>
                  <ul className="ml-3">
                    <li>
                      <p className="item-history-content">
                        Series 7 (General Securities Representative)
                      </p>
                    </li>
                    <li>
                      <p className="item-history-content">
                        Series 24 (General Securities Principal)
                      </p>
                    </li>
                    <li>
                      <p className="item-history-content">
                        Series 66 (Registered Investment Adviser)
                      </p>
                    </li>
                  </ul>
                  <li>
                    <p>Financial Planning</p>
                    <ul className="ml-3">
                      <li>
                        <p className="item-history-content">
                          CFP Board – CERTIFIED FINANCIAL PLANNER&#8482;
                        </p>
                      </li>
                    </ul>
                  </li>
                  <li>
                    <p>Risk Management</p>
                    <ol className="">
                      <li>
                        <p className="item-history-content">
                          Authorized Representative – Life and Variable
                        </p>
                      </li>
                    </ol>
                  </li>
                </ul>
              </li>
              <li>
                <p>Laura S. González</p>
                <ul className="ml-1">
                  <li>
                    <p>Risk Management</p>
                  </li>
                  <ul className="ml-3">
                    <li>
                      <p className="item-history-content">
                        Authorized Representative – Life, Health, Property and
                        Casualty
                      </p>
                    </li>
                  </ul>
                </ul>
              </li>
            </ol>
          </div>
        </TabPane>
      </TabContent>
      {/* <section className="section-bg-wrapper bg-commitment">
				<div className="sub-header-container">
					{/* <h3>OUR COMMITMENT</h3> }
					<ol className="breadcrumb">
 						<li>
 							<Link to="/"><i className="fa fa-home"></i> HOME</Link>
						 </li>
						 <li>
							<Link to="/"><i className=""></i> A 360 APPROACH</Link>
						</li>
 						<li className="active">OUR COMMITMENT</li>
 					</ol>
				</div>
			</section> */}
      {/* <!-- /Sub HEader --> */}

      {/* <section>
				<div className="container">
					<div className="row">
						<div className="col-md-12">
							<div className="main-page">
								<div className="history-list-warp">
									<div className="item-history-post">
										<p className="title-history"><span>INDEPENDENCE</span></p>
										<p className="item-history-content">
											Our alliances are based on the needs of our clients and we have established these 
											as independent contracts. This means that we have a duty to review each and every 
											entity that we do business with, in order to evaluate the performance and level of 
											service provided.
										</p>
									</div>
									
									
									<div className="item-history-post">
										<p className="title-history"><span>OBJECTIVITY</span></p>
										<ol>
											<li>
												<p className="item-history-content">
													As a CERTIFIED FINANCIAL PLANNER&#8482;, Jeffrey Rodriguez owes a fiduciary duty 
													to his clients. As a result, every action and recommendation that we make 
													is geared towards achieving the financial goals and providing for the financial 
													needs of the clients.
												</p>
											</li>
											<li>
												<p className="item-history-content">
													As an independent firm we have no proprietary products and no corporate 
													agenda. This provides reassurance to our clients that our only commitment 
													is their financial success.
												</p>
											</li>
										</ol>

									</div>

									
								</div>
							</div>
						</div> */}
      {/* <!-- <div className="col-md-3">
							<div className="sideabar">
								<div className="widget widget-sidebar widget-list-link">
									<h4 className="title-widget-sidebar">
										Company
									</h4>
									<ul className="wd-list-link">
										<li><a href="#">About Us</a></li>
										<li><a href="#">History</a></li>
										<li><a href="#">Our Advisor</a></li>
										<li><a href="#">Career</a></li>
										<li><a href="#">Partners</a></li>
										<li><a href="#">CEO Message</a></li>
										<li><a href="#">Testimonials</a></li>
										<li><a href="#">Pricing</a></li>

									</ul>
								</div>
								<div className="widget widget-sidebar widget-text-block">
									<h4 className="title-widget-sidebar">
										Company in Lines
									</h4>
									<div className="wd-text-warp">
										<p>Temporibus autem quibusdam et aut officiis debitis is  necessitatibus saepes eveniet ut et seo repudiandae sint et molestiae non Creating futures seon through world.</p>
										<a href="#" className="ot-btn btn-main-color" >
											<i className="fa fa-download" aria-hidden="true"></i>
											Download Presentation</a>
									</div>
								</div>
								<div className="widget-sidebar widget widget-html">
									<div className="wd-html-block">
										<a href="#">
											<img src="http://placehold.it/269x180/ccc.jpg" className="img-responsive" alt="Image">
										</a>
										<div className="content-wd-html-inner">
											<span>HIRING</span>
											<p>
												COME TO JOIN OUR TEAM !
											</p>
										</div>
										<a href="#" className="ot-btn btn-sub-color" >
											Join Now
										</a>
									</div>
								</div>
							</div>
						</div> --> */}
      {/* </div>
				</div>
			</section> */}
    </div>
  );
};

export default OurCommitment;
