import React from "react"
// import {Link} from "react-router-dom"

const FinancialPlanning = (props) => {
    return (
        <div>
            
			<section>
				<div className="container">
					<div className="row">
						<div className="col-md-10">
							<div className="row main-page">
								<div className="col-md-12">
									<div className="list-team-v2">
										<div className="item-team-v2">
											<div className="team-ava">
												<img src="/images/financial-planning/cash_management.png" alt="Cash Cent" className="financial-img"/>
											</div>
											<div className="box-info">
												<h3>Cash Management</h3>
												<h5>We help our clients develop a cash management system that allows them to accomplish the following:</h5>
												<ol>
													<li className="text-md">Establishing a healthy emergency fund</li>
													<li className="text-md">Establishing a budget that considers their short and long term goals</li>
													<li className="text-md">Developing and implementing a debt reduction plan</li>
													<li className="text-md">Developing and maintaining personal financial statements</li>
												</ol>
											</div>
										</div>
										<div className="item-team-v2">
											<div className="team-ava">
												<img src="./images/financial-planning/education.png" alt="Education" className="financial-img"/>
											</div>
											<div className="box-info">
												<h3>Education</h3>
												<ol>
													<li className="text-md">It’s important that our clients establish educational plans for their children preferably from an early age. With this being said, it’s never too late to start the process and provide the gift of education.</li>
													<li className="text-md">The process consists of identifying the expected capital need for the duration of the period for each children. Following this calculation, we evaluate available resources that allows us to invest in the most tax efficient way. Lastly, we establish a disciplined saving system tailored to the achieve the capital requirement.</li>
												</ol>
											</div>
										</div>
										<div className="item-team-v2">
											<div className="team-ava">
												<img src="./images/financial-planning/retirement_planning.png" alt="Retirement" className="financial-img"/>
											</div>
											<div className="box-info">
												<h3>Retirement</h3>
												<ol>
													<li className="text-md">Establishing a healthy emergency fund</li>
													<li className="text-md">Establishing a budget that considers their short and long term goals</li>
													<li className="text-md">Developing and implementing a debt reduction plan</li>
													<li className="text-md">Developing and maintaining personal financial statements</li>
												</ol>
											</div>
										</div>
								</div>
							</div>
						</div>
					</div>
					{/* <div className="col-md-2">
							<div className="sideabar">
								<div className="widget widget-sidebar widget-list-link">
									<h4 className="title-widget-sidebar">
										Company
									</h4>
									<ul className="wd-list-link">
										<li><Link to="about">About Us</Link></li>
										<li><Link to="our-history">Our History</Link></li>
									</ul>
								</div>
							</div>
						</div> */}
                </div>
                </div>
			</section>
        </div>
    )
}

export default FinancialPlanning