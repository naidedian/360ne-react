import React from "react";
// import { Link } from "react-router-dom";

const RiskManagement = props => {
  return (
    <div>
      <section>
        <div className="statements-warp">
          <h2>Risk Management</h2>
          <div className="statements-accordion">
            <div className="panel-group accordion-2" id="accordion">
              <div className="panel panel-default">
                <div className="panel-heading">
                  <h4 className="panel-title">
                    <a
                      className="accordion-toggle collapsed"
                      data-toggle="collapse"
                      data-parent="#accordion"
                      href="#collapseOne"
                    >
                      LIFE
                    </a>
                  </h4>
                </div>
                <div id="collapseOne" className="panel-collapse collapse">
                  <div className="panel-body">
                    <ul className="unorder-collapse-content">
                      <li className="collapse-item">
                        <h3>WHOLELIFE:</h3>
                        <p>
                          Is a life insurance policy which is guaranteed to
                          remain in force for the insured’s entire lifetime,
                          provided required premiums are paid, or to the
                          maturity date. It is a powerful way to protect your
                          love ones. It is a tool that you have to build cash
                          and increase and maximized your financial goals. The
                          insured party normally pays premiums until death,
                          except for limited pay policies which may be paid-up
                          in 10 years, 20 years, at age 65 and up to age 100.
                        </p>
                      </li>
                      <li className="collapse-item">
                        <h3>TERM:</h3>
                        <p>
                          A policy with a set duration limit on the coverage
                          period. Once the policy is expired, it is up to the
                          policy owner to decide whether to renew the term life
                          insurance policy or to let the coverage end.
                        </p>
                      </li>
                      <li className="collapse-item">
                        <h3>UNIVERSAL:</h3>
                        <p>
                          A type of flexible permanent life insurance offering
                          the low-cost protection of term life insurance as well
                          as a savings element (like whole life insurance) which
                          is invested to provide a cash value buildup. The death
                          benefit, savings element and premiums can be reviewed
                          and altered as a policyholder’s circumstances change.
                          In addition, unlike whole life insurance, universal
                          life insurance allows the policyholder to use the
                          interest from his or her accumulated savings to help
                          pay premiums.
                        </p>
                      </li>
                      <li className="collapse-item">
                        <h3>VARIABLE:</h3>
                        <p>
                          Variable universal life insurance (VUL) is a form of
                          cash-value life insurance that offers both a death
                          benefit and an investment feature. The premium amount
                          for variable universal life insurance (VUL) is
                          flexible and may be changed by the consumer as needed,
                          though these changes can result in a change in the
                          coverage amount. The investment feature usually
                          includes “sub-accounts,” which function very similar
                          to mutual funds and can provide exposure to stocks and
                          bonds. This exposure offers the possibility of an
                          increased rate of return over a normal universal life
                          or permanent insurance policy.
                        </p>
                      </li>
                    </ul>
                  </div>
                </div>
              </div>
              <div className="panel panel-default">
                <div className="panel-heading">
                  <h4 className="panel-title">
                    <a
                      className="accordion-toggle collapsed"
                      data-toggle="collapse"
                      data-parent="#accordion"
                      href="#collapseTwo"
                    >
                      DISABILITY
                    </a>
                  </h4>
                </div>
                <div id="collapseTwo" className="panel-collapse collapse">
                  <div className="panel-body">
                    <ul className="unorder-collapse-content">
                      <li className="collapse-item">
                        <h3>DISABILITY INCOME:</h3>
                        <p>
                          An insurance product that provides supplementary
                          income in the event of an illness or accident
                          resulting in a disability that prevents the insured
                          from working at their regular employment. Benefits are
                          usually provided on a monthly basis so that the
                          individual can maintain their standard of living and
                          continue to pay their regular expenses.
                        </p>
                      </li>
                    </ul>
                  </div>
                </div>
              </div>
              <div className="panel panel-default">
                <div className="panel-heading">
                  <h4 className="panel-title">
                    <a
                      className="accordion-toggle collapsed"
                      data-toggle="collapse"
                      data-parent="#accordion"
                      href="#collapseThree"
                    >
                      PROPERTY & CASUALTY
                    </a>
                  </h4>
                </div>
                <div id="collapseThree" className="panel-collapse collapse">
                  <div className="panel-body">
                    <ol className="order-collapse-content">
                      <li>Personal</li>
                      <li>Personal Auto</li>
                      <li>Commercial</li>
                      <li>Auto Commercial</li>
                      <li>Marine Insurance</li>
                      <li>Title insurance</li>
                      <li>Mal Practice</li>
                      <li>Errors & Omissions</li>
                      <li>Professional Liability</li>
                      <li>Bonds</li>
                    </ol>
                  </div>
                </div>
              </div>
            </div>
          </div>
          <ul className="mt-3 list-check-icon">
            <li>
              <h4>Insurance Needs Analysis</h4>
            </li>
            <li>
              <h4>Types of Coverage and Products</h4>
            </li>
            <li>
              <h4>Complimentary Policy Review</h4>
            </li>
          </ul>
        </div>
      </section>
      {/* <div className="test-border">
				<div className="test-border__item"></div>
				<div className="bg-image">
					<h1>A 360 APPROACH</h1>
				</div>
				<div className="test-border__item test-border--rotate"></div>
				<div className="text-block text-center">
					<p className="mb-2">
						We live in a world of constant change. The social, cultural, political and the financial aspects of our lives are a
						few of the many elements that forces us to adapt and adjust accordingly. Individuals and in many cases
						institutions, establish financial strategies that consider the elements presented at a particular time. Many of
						those strategies fail to reach the established goals for two reasons; First, The strategy was not reviewed
						under a systematic and holistic approach. Second, the strategy was implemented without considering the
						other areas that affect an individual or an institution’s financial life.
					</p>
					<Link to="/lets-talk" className="cta-btn mb-2">LET'S TALK</Link>
				</div>
			</div> */}
    </div>
  );
};

export default RiskManagement;
