import React from "react";
import FinancialPlanning from "./FinancialPlanning";
import InstitutionalBusinessSolutions from "./InstitutionalBusinessSolutions";
import RiskManagement from "./RiskManagement";
import InvestmentManagement from "./InvestmentManagement";
import { Tab, TabPane, TabContent, TabTitle } from "../Tab";

const Approach = (props) => (
	<>
		<div className="">
			{/* <section className="section-bg-wrapper bg-approach"></section> */}
			<Tab>
				<TabTitle link="financial" activeClass="active">
					FINANCIAL PLANNING
				</TabTitle>
				<TabTitle link="investment">INVESTMENT MANAGEMENT</TabTitle>
				<TabTitle link="risk">RISK MANAGEMENT</TabTitle>
				<TabTitle link="institutional">INST & BUS SOLU</TabTitle>
			</Tab>
			<TabContent className="width-1024 center-block">
				<TabPane id="financial" activePane="active">
					<FinancialPlanning />
				</TabPane>
				<TabPane id="investment">
					<InvestmentManagement />
				</TabPane>
				<TabPane id="risk">
					<RiskManagement />
				</TabPane>
				<TabPane id="institutional">
					<InstitutionalBusinessSolutions />
				</TabPane>
			</TabContent>
		</div>
		<section className="section-bg-wrapper bg-partners"></section>
	</>
);

export default Approach;
