import React from "react"
// import {Link} from "react-router-dom"

const InvestmentManagement = (props) => {
    return (
        <div>
			<section>
				<div className="container">
					<div className="row">

						{/* <div className="category-case-warp">
							<div className="casesFilter">
                                <a href="#" data-filter="*" className="current ">ALL</a>
                                <a href="#" data-filter=".Products" className="">Products</a>
                                <a href="#" data-filter=".Services" className="">Services</a>
                      		</div> 
                              <!-- End Project Fillter -->
						</div> */}
						<div className="icon-flex-grid">
							<div className=" Products">
								<div className="">
									<div className="investment-item">
									<img src="./images/icon/stock.png" className="d-inline-block" alt="Stocks" width="250"/>
										{/* <h5 className="d-inline-block">Stocks</h5> */}
									</div>
								</div>
							</div>
							<div className=" Products">
								<div className="">
									<div className="investment-item">
									<img src="./images/icon/etf.png" className="d-inline-block" alt="ETF" width="250"/>
										{/* <h5 className="d-inline-block">ETFs</h5> */}
									</div>
								</div>
							</div>
							<div className=" Products">
								<div className="">
									<div className="investment-item">
									    <img src="./images/icon/bonds.png" className="d-inline-block" alt="Bonds" width="250"/>
										{/* <h5 className="d-inline-block">Bonds</h5> */}
									</div>
								</div>
							</div>
							<div className=" Products">
								<div className="">
									<div className="investment-item">
									    <img src="./images/icon/alternative_investment.png" className="d-inline-block" alt="Alternative Investing" width="250"/>
										{/* <h5 className="d-inline-block">Alternative Investments</h5> */}
									</div>
								</div>
							</div>
							<div className=" Products">
								<div className="">
									<div className="investment-item">
									    <img src="./images/icon/mutual_funds.png" className="d-inline-block" alt="Mutual Fund" width="250"/>
										{/* <h5 className="d-inline-block">Mutual Funds</h5> */}
									</div>
								</div>
							</div>
							<div className=" Products">
								<div className="">
									<div className="investment-item">
									    <img src="./images/icon/reit.jpg" className="d-inline-block" alt="REIT" width="250"/>
										{/* <h5 className="d-inline-block">Real Estate Investment Trusts (REIT)</h5> */}
									</div>
								</div>
							</div>
							<div className=" Products">
								<div className="">
									<div className="investment-item">
									    <img src="./images/icon/variable_insurance.png" className="d-inline-block" alt="Insurance" width="250"/>
										{/* <h5 className="d-inline-block">Variable Insurance Products</h5> */}
									</div>
								</div>
							</div>
							<div className=" Services">
								<div className="">
									<div className="investment-item">
									<img src="./images/icon/lending_solutions.png" className="d-inline-block" alt="Lend" width="250"/>
									<div className="d-inline-block">
										{/* <h5 className="d-inline-block">Lending Solutions</h5> */}
									</div>	
									</div>
								</div>
							</div>
							<div className=" Services">
								<div className="">
									<div className="investment-item">
										<img src="./images/icon/brokerage_accounts.png" className="d-inline-block" alt="Brokerage" width="250"/>
										{/* <h5 className="d-inline-block">Brokerage Accounts</h5> */}
									</div>
								</div>
							</div>
							<div className="  Services">
								<div className="">
									<div className="investment-item">
										<img src="./images/icon/research_economic.png" className="d-inline-block" alt="Research" width="250"/>
										{/* <h5 className="d-inline-block">Research and Economic Commentary</h5> */}
									</div>
								</div>
							</div>
							<div className=" Services">
									<div className="investment-item">
										<img src="./images/icon/asset_management.png" className="d-inline-block" alt="Asset" width="250"/>
										<div className="d-inline-block">
											{/* <h5>Asset Management</h5> */}
										</div>
									</div>
							</div>
							
						</div>

					</div>
					<h3>Investment Policy Creation and Review</h3>
				<div className="">
					<div className="">
						<div className="investment-item flex-dir-col">
							<div className="investment-list-item"><p>Every investment portfolio that we design and implement must follow an investment policy. The creation of an investment policy will consider numerous elements that are of relevance to the client/investor, as well as external factors. The policy determines the level of risk that the portfolio can sustain, the appropriate mix of asset classes and the level of income required by the investor. Also included are the evaluation metrics that will be used to determine if the portfolio is performing according to the needs and objectives of the investor.</p></div>
							<div className="investment-list-item"><p>An Investment Policy should be reviewed periodically to reflect changes in internal and external factors. Regulatory, economic and market conditions are some of the issues that must be considered when reviewing an IP.</p></div>
						</div>
					</div>
				</div>
				</div>
				
			</section>
			{/* <!-- /Cases --> */}
        </div>
    )
}

export default InvestmentManagement