import React from "react";
// import { Link } from "react-router-dom";

const InstitutionalBusinessSolutions = (props) => {
	return (
		<div>
			<section className="no-padding">
				<div className="container">
					<div className="row">
						<div className="col-md-12 pb-2">
							<div className="title-block title-contact">
								<h3>Institutional and Business Solutions</h3>
								<span className="bottom-title"></span>
							</div>
							<div className="row">
								<div className="job-detail-info-warp">
									<div className="col-md-6">
										<h3>Retirement Consulting</h3>
										<div>
											<h4>Qualified Plans</h4>
											<ul className="list-check-icon">
												<li>Profit Sharing</li>
												<li>Money Purchase</li>
												<li>401K</li>
												<li>Keogh Plans</li>
											</ul>
										</div>
										<div>
											<h4>Non Qualified Plans</h4>
											<ul className="list-check-icon">
												<li>
													<a
														href="http://www.investopedia.com/terms/d/deferred-compensation.asp"
														target="_blank"
														rel="noopener noreferrer"
													>
														Deferred compensation
													</a>{" "}
													plans
												</li>
												<li>Executive bonus plans</li>
												<li>Group carve-out plans</li>
												<li>
													Split-dollar{" "}
													<a
														href="http://www.investopedia.com/terms/l/lifeinsurance.asp"
														target="_blank"
														rel="noopener noreferrer"
													>
														life insurance
													</a>{" "}
													plans
												</li>
											</ul>
										</div>
									</div>
									<div className="col-md-6">
										<h3>Institutional Consulting</h3>
										<div>
											<h4>Asset Management</h4>
											<ul className="list-check-icon">
												<li>Investment Policy Design and Review</li>
												<li>
													Portfolio Management
													<ol className="sub-list">
														<li>Brokerage accounts</li>
														<li>Managed accounts</li>
													</ol>
												</li>
												<li>Fixed Income Portfolio Analysis</li>
												<li>Investment Committee Presentations</li>
											</ul>
										</div>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
			</section>
		</div>
	);
};

export default InstitutionalBusinessSolutions;
