import React from "react";
// import { Link } from "react-router-dom";
import { Tab, TabTitle, TabContent, TabPane } from "./Tab/index";

const About = (props) => {
	return (
		<div id="ourfirm" className="section-separation">
			<section className="section-bg-wrapper bg-firm"></section>
			<Tab>
				<TabTitle link="firm" activeClass="active">
					OUR FIRM
				</TabTitle>
			</Tab>
			<TabContent className="width-1024 center-block">
				<TabPane id="firm" activePane="active">
					<p>
						We help individuals and institutions with financial planning,
						investment management and advisory services.
					</p>
				</TabPane>
			</TabContent>
		</div>
		// <div>
		//     <section className="no-padding sh-about">
		// 		<div className="sub-header ">
		// 			<h3>ABOUT US</h3>
		// 			<ol className="breadcrumb">
		// 				<li>
		// 					<Link to="/"><i className="fa fa-home"></i> HOME</Link>
		// 				</li>
		// 				<li>
		// 					<Link to="/">A 360 APPROACH</Link>
		// 				</li>
		// 				<li className="active">ABOUT US</li>
		// 			</ol>
		// 		</div>
		// 	</section>
		// 	{/* <!-- /Sub header --> */}

		// 	<section className="bg-theme partner-h8-warp">
		// 		<div className="container">
		// 			<div className="row">
		// 				<div id="partner-h2" className="owl-partner-h8">
		// 					<div className="item-partner item-partner-h8" >
		// 						<Link to="#"><img src="http://placehold.it/119x94/ccc.jpg" className="img-responsive partner-img" alt="360ne"/></Link>
		// 					</div>
		// 					<div className="item-partner item-partner-h8" >
		// 						<Link to="#"><img src="http://placehold.it/119x94/ccc.jpg" className="img-responsive partner-img" alt="360ne"/></Link>
		// 					</div>
		// 					<div className="item-partner item-partner-h8" >
		// 						<Link to="#"><img src="http://placehold.it/142x105/ccc.jpg" className="img-responsive partner-img" alt="360ne"/></Link>
		// 					</div>
		// 					<div className="item-partner item-partner-h8" >
		// 						<Link to="#"><img src="http://placehold.it/126x127/ccc.jpg" className="img-responsive partner-img" alt="360ne"/></Link>
		// 					</div>
		// 					<div className="item-partner item-partner-h8" >
		// 						<Link to="#"><img src="http://placehold.it/119x94/ccc.jpg" className="img-responsive partner-img" alt="360ne"/></Link>
		// 					</div>
		// 					<div className="item-partner item-partner-h8" >
		// 						<Link to="#"><img src="http://placehold.it/119x94/ccc.jpg" className="img-responsive partner-img" alt="360ne"/></Link>
		// 					</div>
		// 					<div className="item-partner item-partner-h8" >
		// 						<Link to="#"><img src="http://placehold.it/142x105/ccc.jpg" className="img-responsive partner-img" alt="360ne"/></Link>
		// 					</div>
		// 					<div className="item-partner item-partner-h8" >
		// 						<Link to="#"><img src="http://placehold.it/126x127/ccc.jpg" className="img-responsive partner-img" alt="360ne"/></Link>
		// 					</div>
		// 				</div>
		// 			</div>
		// 		</div>
		// 	</section>
		// 	{/* <!-- /Partner --> */}

		// 	<section className="bg-strategy-h4">
		// 		<div className="container">
		// 			<div className="row">
		// 				<div className="col-md-12">
		// 					<div className="strategy-warp-h4">
		// 						<div className="title-block text-center">
		// 							<span className="top-title"></span>
		// 							<h2>Our Commitment</h2>
		// 							<p className="sub-title">As always, partners grows with you!</p>
		// 							<span className="bottom-title"></span>
		// 						</div>
		// 						<p className="demo-sub-about-text-4">Temporibus autem quibusdam et aut officiis debitis is aut rerum necessitatibus saepes eveniet ut etes seo lage voluptates repudiandae sint et molestiae non mes  for Creating  futures through building pres preservation.</p>
		// 						<div className="col-md-4">
		// 							<div className="iconbox text-center">
		// 								<span className="icon icon-pie-chart"></span>
		// 								<h4>Cash Flow</h4>
		// 								<p>Lorem ipsum dolor sit , consectet adipisi elit, sed do eiusmod tempor for enim adesg ens minim veniam.</p>
		// 							</div>
		// 							<div className="iconbox text-center no-mgb">
		// 								<span className="icon icon-library"></span>
		// 								<h4>Investments</h4>
		// 								<p>Lorem ipsum dolor sit , consectet adipisi elit, sed do eiusmod tempor for enim adesg ens minim veniam.</p>
		// 							</div>
		// 						</div>
		// 						<div className="col-md-4">
		// 							<div className="iconbox text-center">
		// 								<span className="icon icon-coin-dollar"></span>
		// 								<h4>Purchases</h4>
		// 								<p>Lorem ipsum dolor sit , consectet adipisi elit, sed do eiusmod tempor for enim adesg ens minim veniam.</p>
		// 							</div>
		// 							<div className="iconbox text-center no-mgb">
		// 								<span className="icon icon-user-tie"></span>
		// 								<h4>Considerations</h4>
		// 								<p>Lorem ipsum dolor sit , consectet adipisi elit, sed do eiusmod tempor for enim adesg ens minim veniam.</p>
		// 							</div>
		// 						</div>
		// 						<div className="col-md-4">
		// 							<div className="iconbox text-center">
		// 								<span className="icon icon-stats-bars2"></span>
		// 								<h4>Collections</h4>
		// 								<p>Lorem ipsum dolor sit , consectet adipisi elit, sed do eiusmod tempor for enim adesg ens minim veniam.</p>
		// 							</div>
		// 							<div className="iconbox text-center no-mbg">
		// 								<span className="icon icon-trophy"></span>
		// 								<h4>Professional</h4>
		// 								<p>Lorem ipsum dolor sit , consectet adipisi elit, sed do eiusmod tempor for enim adesg ens minim veniam.</p>
		// 							</div>
		// 						</div>
		// 					</div>
		// 				</div>
		// 			</div>
		// 		</div>
		// 	</section>
		// 	{/* <!-- /strategy --> */}

		// 	<section>
		// 		<div className="container">
		// 			<div className="row">
		// 				<div className="col-md-12">
		// 					<div className="title-block text-center">
		// 						<span className="top-title"></span>
		// 						<h2>Our Team</h2>
		// 						<p className="sub-title">As always, partners grows with you!</p>
		// 						<span className="bottom-title"></span>
		// 					</div>
		// 					<div className="warp-full-width team-h4-warp">
		// 						<div id="team" className="owl-team owl-team-h4">
		// 							<div className="item-team">
		// 								<img src="http://placehold.it/269x265/ccc.jpg" className="img-responsive" alt="360ne"/>
		// 								<div className="body-team">
		// 									<h4>Fatma Mahmoud</h4>
		// 									<p>Financial advisor</p>
		// 								</div>
		// 								<div className="footer-team">
		// 									<a className="facebook" href="https://facebook.com"><i className="fa fa-facebook"></i></a>
		// 									<a className="twitter" href="https://twitter.com"><i className="fa fa-twitter"></i></a>
		// 								</div>
		// 							</div>
		// 							<div className="item-team">
		// 								<img src="http://placehold.it/269x265/ccc.jpg" className="img-responsive" alt="360ne"/>
		// 								<div className="body-team">
		// 									<h4>Ahmed Magdy</h4>
		// 									<p>LOANS advisor</p>
		// 								</div>
		// 								<div className="footer-team">
		// 								<a className="facebook" href="https://facebook.com"><i className="fa fa-facebook"></i></a>
		// 									<a className="twitter" href="https://twitter.com"><i className="fa fa-twitter"></i></a>
		// 								</div>
		// 							</div>
		// 							<div className="item-team">
		// 								<img src="http://placehold.it/269x265/ccc.jpg" className="img-responsive" alt="360ne"/>
		// 								<div className="body-team">
		// 									<h4>Amany Abody</h4>
		// 									<p>BONDS advisor</p>
		// 								</div>
		// 								<div className="footer-team">
		// 								<a className="facebook" href="https://facebook.com"><i className="fa fa-facebook"></i></a>
		// 									<a className="twitter" href="https://twitter.com"><i className="fa fa-twitter"></i></a>
		// 								</div>
		// 							</div>
		// 							<div className="item-team">
		// 								<img src="http://placehold.it/269x265/ccc.jpg" className="img-responsive" alt="360ne"/>
		// 								<div className="body-team">
		// 									<h4>Ali Mahmoud</h4>
		// 									<p>Financial advisor</p>
		// 								</div>
		// 								<div className="footer-team">
		// 								<a className="facebook" href="https://facebook.com"><i className="fa fa-facebook"></i></a>
		// 									<a className="twitter" href="https://twitter.com"><i className="fa fa-twitter"></i></a>
		// 								</div>
		// 							</div>
		// 							<div className="item-team">
		// 								<img src="http://placehold.it/269x265/ccc.jpg" className="img-responsive" alt="360ne"/>
		// 								<div className="body-team">
		// 									<h4>Fatma Mahmoud</h4>
		// 									<p>Financial advisor</p>
		// 								</div>
		// 								<div className="footer-team">
		// 								<a className="facebook" href="https://facebook.com"><i className="fa fa-facebook"></i></a>
		// 									<a className="twitter" href="https://twitter.com"><i className="fa fa-twitter"></i></a>
		// 								</div>
		// 							</div>
		// 							<div className="item-team">
		// 								<img src="http://placehold.it/269x265/ccc.jpg" className="img-responsive" alt="360ne"/>
		// 								<div className="body-team">
		// 									<h4>Ahmed Magdy</h4>
		// 									<p>LOANS advisor</p>
		// 								</div>
		// 								<div className="footer-team">
		// 								<a className="facebook" href="https://facebook.com"><i className="fa fa-facebook"></i></a>
		// 									<a className="twitter" href="https://twitter.com"><i className="fa fa-twitter"></i></a>
		// 								</div>
		// 							</div>
		// 							<div className="item-team">
		// 								<img src="http://placehold.it/269x265/ccc.jpg" className="img-responsive" alt="360ne"/>
		// 								<div className="body-team">
		// 									<h4>Amany Abody</h4>
		// 									<p>BONDS advisor</p>
		// 								</div>
		// 								<div className="footer-team">
		// 								<a className="facebook" href="https://facebook.com"><i className="fa fa-facebook"></i></a>
		// 									<a className="twitter" href="https://twitter.com"><i className="fa fa-twitter"></i></a>
		// 								</div>
		// 							</div>
		// 							<div className="item-team">
		// 								<img src="http://placehold.it/269x265/ccc.jpg" className="img-responsive" alt="360ne"/>
		// 								<div className="body-team">
		// 									<h4>Ali Mahmoud</h4>
		// 									<p>Financial advisor</p>
		// 								</div>
		// 								<div className="footer-team">
		// 								<a className="facebook" href="https://facebook.com"><i className="fa fa-facebook"></i></a>
		// 									<a className="twitter" href="https://twitter.com"><i className="fa fa-twitter"></i></a>
		// 								</div>
		// 							</div>
		// 							<div className="item-team">
		// 								<img src="http://placehold.it/269x265/ccc.jpg" className="img-responsive" alt="360ne"/>
		// 								<div className="body-team">
		// 									<h4>Ali Mahmoud</h4>
		// 									<p>Financial advisor</p>
		// 								</div>
		// 								<div className="footer-team">
		// 									<a className="facebook" href="https://facebook.com"><i className="fa fa-facebook"></i></a>
		// 									<a className="twitter" href="https://twitter.com"><i className="fa fa-twitter"></i></a>
		// 								</div>
		// 							</div>
		// 						</div>

		// 						<div className="customNavigation">
		// 			                <Link to="#" className="btn-2 prev-team prev-team-h4"><i className="fa fa-angle-left"></i></Link>
		// 			                <Link to="#" className="btn-2 next-team next-team-h4"><i className="fa fa-angle-right"></i></Link>
		// 			        	</div>
		//                         {/* <!-- End owl button --> */}
		// 					</div>
		// 				</div>
		// 			</div>
		// 		</div>
		// 	</section>
		// </div>
	);
};

export default About;
