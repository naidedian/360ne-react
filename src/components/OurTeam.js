import React from "react";
// import { Link } from "react-router-dom";

const OurTeam = props => {
  return (
    <div>
      {/* <section className="bg-team section-bg-wrapper">
				<div className="sub-header-container">
					<h3>OUR TEAM</h3>
					<ol className="breadcrumb">
 						<li>
 							<Link to="/"><i className="fa fa-home"></i> HOME</Link>
 						</li>
 						<li>
 							<Link to="/"><i className=""></i> A 360 APPROACH</Link>
 						</li>
 						
 						<li className="active">OUR TEAM</li>
 					</ol>
				</div>
			</section> */}
      {/* <!-- /subheader --> */}

      <section>
        <div className="container">
          <div className="row">
            <div className="col-md-12">
              <div className="main-page">
                <div className="about-founder">
                  <div className="box-img-left">
                    <div className="feature">
                      <img
                        src="/images/Team/jeffrey_rodriguez.png"
                        className="img-responsive "
                        alt="Jeffrey Rodriguez"
                      />
                    </div>
                    <div className="boxcontent">
                      <h4>Jeffrey Rodriguez, CFP&reg;</h4>
                      <ul className="list-check-icon circle team-content-container">
                        <li>
                          <p>
                            Mr. Rodriguez has been helping his clients in
                            financial planning, investment management and
                            institutional consulting since 2003. In 2010, he
                            made the decision of establishing an independent
                            financial services firm with the sole purpose of
                            providing his clients an open platform that was
                            centered in his client’s best interests. Through IFS
                            Securities, Mr. Rodriguez is able to provide
                            professional investment management to his clients
                            and have the support of our selected custodian
                            Raymond James Financial Inc., a company that was
                            *ranked as the best full service broker dealer for
                            2011 by Smart Money on their June 2011 edition.
                          </p>
                        </li>
                        <li>
                          <p>
                            He holds a bachelor degree in Public Accounting from
                            the Pontifical Catholic University of Puerto Rico, a
                            Masters Degree in Global Management from the
                            University of Phoenix and a Financial Planning
                            Certificate from Kaplan University.
                          </p>
                        </li>
                        <li>
                          <p>
                            In the year 2015 the{" "}
                            <i>
                              Certified Financial Planner Board of Standards
                              certified Mr. Jeffrey Rodriguez as a CERTIFIED
                              FINANCIAL PLANNER™.
                            </i>
                          </p>
                        </li>
                        <li>
                          <p>
                            The designation comes with extensive training in
                            financial planning, estate planning, insurance,
                            investments, taxes, employee benefits and retirement
                            planning, as well as in CFP Board’s{" "}
                            <i>Standards of Professional Conduct</i>, which are
                            rigorously enforced. As a CFP&reg;; professional,
                            Mr. Rodriguez is required to uphold the
                            certification through continuing education –
                            something to consider with new financial instruments
                            appearing regularly on the consumer market. In fact,
                            CFP&reg;; certification is the most recognized in
                            the industry for personal financial planning. Only
                            17% of all financial advisors in the industry can
                            claim this distinction.For more details please visit
                            <a
                              href="http://www.cfp.org"
                              target="_blank"
                              rel="noopener noreferrer"
                            >
                              {" "}
                              www.cfp.org
                            </a>
                            .
                          </p>
                        </li>
                      </ul>
                    </div>
                  </div>
                </div>
                {/* <!-- /about founder --> */}
                {/* <div className="ourteam-page">
									<div className="title-block text-center title-pd ">
										<span className="top-title"></span>
										<h2>Our Team</h2>
										<p className="sub-title">As always, partners grows with you!</p>
										<span className="bottom-title"></span>
									</div>
									<div className="row">
										<div className="col-md-4 col-sm-6">
											<div className="item-team">
												<img src="https://picsum.photos/269/265" className="img-responsive" alt="360ne"/>
												<div className="body-team">
													<h4>Laura S. Gonzalez Cobos</h4>
													<p>Financial advisor</p>
												</div>
												<div className="footer-team">
													<a className="facebook" href="https://facebook.com"><i className="fa fa-facebook"></i></a>
													<a className="twitter" href="https://twitter.com"><i className="fa fa-twitter"></i></a>
												</div>
											</div>
										</div>
										<div className="col-md-4 col-sm-6">
											<div className="item-team">
												<img src="https://picsum.photos/269/265" className="img-responsive" alt="360ne"/>
												<div className="body-team">
													<h4>Ahmed Magdy</h4>
													<p>LOANS advisor</p>
												</div>
												<div className="footer-team">
												<a className="facebook" href="https://facebook.com"><i className="fa fa-facebook"></i></a>
													<a className="twitter" href="https://twitter.com"><i className="fa fa-twitter"></i></a>
												</div>
											</div>
										</div>
										<div className="col-md-4 col-sm-6">
											<div className="item-team">
												<img src="https://picsum.photos/269/265" className="img-responsive" alt="360ne"/>
												<div className="body-team">
													<h4>Amany Abody</h4>
													<p>BONDS advisor</p>
												</div>
												<div className="footer-team">
												<a className="facebook" href="https://facebook.com"><i className="fa fa-facebook"></i></a>
													<a className="twitter" href="https://twitter.com"><i className="fa fa-twitter"></i></a>
												</div>
											</div>
										</div>
									</div>
								</div> */}
              </div>
            </div>
            {/* <!-- <div className="col-md-3">
							<div className="sideabar">
								<div className="widget widget-sidebar widget-list-link">
									<h4 className="title-widget-sidebar">
										Company
									</h4>
									<ul className="wd-list-link">
										<li><a href="#">About Us</a></li>
										<li><a href="#">History</a></li>
										<li><a href="#">Our Advisor</a></li>
										<li><a href="#">Career</a></li>
										<li><a href="#">Partners</a></li>
										<li><a href="#">CEO Message</a></li>
										<li><a href="#">Testimonials</a></li>
										<li><a href="#">Pricing</a></li>

									</ul>
								</div>
								<div className="widget widget-sidebar widget-text-block">
									<h4 className="title-widget-sidebar">
										Company in Lines
									</h4>
									<div className="wd-text-warp">
										<p>Temporibus autem quibusdam et aut officiis debitis is  necessitatibus saepes eveniet ut et seo repudiandae sint et molestiae non Creating futures seon through world.</p>
										<a href="#" className="ot-btn btn-main-color" >
											<i className="fa fa-download" aria-hidden="true"></i>
											Download Presentation</a>
									</div>
								</div>
								<div className="widget-sidebar widget widget-html">
									<div className="wd-html-block">
										<a href="#">
											<img src="https://picsum.photos/269/180" className="img-responsive" alt="Image">
										</a>
										<div className="content-wd-html-inner">
											<span>HIRING</span>
											<p>
												COME TO JOIN OUR TEAM !
											</p>
										</div>
										<a href="#" className="ot-btn btn-sub-color" >
											Join Now
										</a>
									</div>
								</div>
							</div>
						</div> --> */}
          </div>
        </div>
      </section>
    </div>
  );
};

export default OurTeam;
