import React from "react";

const Loader = props => {
  return (
    <div className="preload-container">
      <div className="loader-circle"></div>
    </div>
  );
};

export default Loader;
