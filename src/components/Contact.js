import React from "react";

const Contact = (props) => (
	<div>
		{/* <section className="section-bg-wrapper bg-letstalk mt-4"></section> */}
		<section className="cb-h1 no-padding">
			<div className="container pb-2">
				<div className="row">
					<div className="title-block text-center title-pd">
						<span className="top-title "></span>
						<h2>Schedule Meeting Today</h2>
						<p className="sub-title">
							Our financial advisers will always help you
						</p>
						<span className="bottom-title"></span>
					</div>
					<div className="callback-warp cb-h1">
						<div className="col-md-12">
							<div className="form-cb-warp">
								<form className="cb-form">
									<div className="form-group">
										<input
											type="text"
											className="form-control"
											id="name"
											placeholder="Your Name*"
										/>
									</div>

									<select className="cb">
										<option value="">I would like to discuss</option>
										<option>Strategic Decision Making</option>
										<option>International Finance Resources</option>
										<option>Hedging & Risk Management</option>
										<option>International Capital Markets</option>
										<option>Foreign Exchange Markets</option>
										<option>International Finance & Global Markets</option>
										<option>International Finance Tutorial</option>
									</select>

									<div className="form-group">
										<input
											type="email"
											className="form-control"
											placeholder="Your Email*"
										/>
									</div>
									<div className="form-group">
										<input
											type="text"
											className="form-control"
											id="phone"
											placeholder="Your Phone Number*"
										/>
									</div>

									<button type="submit" className="btn-main-color">
										<i className="fa fa-paper-plane" aria-hidden="true"></i>{" "}
										Submit
									</button>
								</form>
							</div>
						</div>
						{/* <div className="col-md-5 hidden-xs hidden-sm">
							<img src="https://picsum.photos/438/559" className="img-responsive" alt="Image"/>
						</div> */}
					</div>
				</div>
			</div>
		</section>
	</div>
);

export default Contact;
