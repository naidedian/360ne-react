import React from "react";
import { Link } from "react-router-dom";

const FallBack = () => (
	<div id="fallback" className="text-center mb-2 flex-column">
		<img
			src="./images/360ne_logo_transparent.png"
			className="img-responive"
			alt="360ne Logo"
			width="250"
		/>
		<h1>
			The page you requested does not exist. <br />
			<span role="img" aria-label="sad">
				&#x1F62D;
			</span>
		</h1>
		<div className="mt-1">
			<Link to="/" className="cta-btn">
				Go Home
			</Link>
		</div>
	</div>
);

export default FallBack;
