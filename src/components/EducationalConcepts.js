import React from "react"
import {Link} from "react-router-dom"

const EducationalConcepts = props => {
    return (
        <div>
            <section className="section-bg-wrapper bg-educational">
			<div className="sub-header-container">
				{/* <h3>EDUCATIONAL CONCEPTS</h3> */}
				<ol className="breadcrumb">
					<li>
						<Link to="/"><i className="fa fa-home"></i> HOME</Link>
					</li>

					<li className="active"> EDUCATIONAL CONCEPTS</li>
				</ol>
			</div>
		</section>
		{/* <!-- /subheader --> */}

		<section>
			<div className="container">
				<div className="row">
					<div className="col-md-12">
						<div className="news-list-warp">
							{/* <!-- Business Succession Planning --> */}
							<div className="item-new-list ">
								{/* <!-- add className no-position --> */}
								<div className="feature-new-warp">
									<Link to="#">
										<img src="./images/educational-concepts/business-succession.jpg" alt="Business Sucession"/>
									</Link>
								</div>
								<div className="box-new-info">
									<div className="new-info">
										<h4>
											<Link to="#">Business Succession Planning</Link>
										</h4>
										<p><i className="fa fa-calendar" aria-hidden="true"></i> July 6, 2016</p>
										<p><i className="fa fa-user" aria-hidden="true"></i> By Admin</p>
									</div>
									<div className="tapo">
										<p>
											When developing a succession plan for your business, you must make many
											decisions. Should you sell your business or give it away? Should you
											structure your plan to go into effect during your lifetime or...
										</p>
									</div>
									<Link to="#" className="ot-btn btn-sub-color">Read More</Link>
								</div>
							</div>
							{/* <!-- Five Questions about Long-Term Care --> */}
							<div className="item-new-list ">
								{/* <!-- add className no-position --> */}
								<div className="feature-new-warp">
									<Link to="#">
										<img src="./images/educational-concepts/long-term-care.jpg" alt="Long Term Care"/>
									</Link>
								</div>
								<div className="box-new-info">
									<div className="new-info">
										<h4>
											<a href="single_new.html">Five Questions about Long-Term Care</a>
										</h4>
										<p><i className="fa fa-calendar" aria-hidden="true"></i> July 6, 2016</p>
										<p><i className="fa fa-user" aria-hidden="true"></i> By Admin</p>
									</div>
									<div className="tapo">
										<p>
											1. What is long-term care? Long-term care refers to the ongoing services and
											support needed by people who have chronic health conditions or disabilities.
											There are three levels of long-term care: Skilled care: Generally
											round-the-clock...
										</p>
									</div>
									<Link to="#" className="ot-btn btn-sub-color">Read More</Link>
								</div>
							</div>
							{/* <!-- Protecting Your Loved Ones with Life Insurance --> */}
							<div className="item-new-list ">
								{/* <!-- add className no-position --> */}
								<div className="feature-new-warp">
									<Link to="#">
										<img src="./images/educational-concepts/insurance.jpg" alt="Insurance"/>
									</Link>
								</div>
								<div className="box-new-info">
									<div className="new-info">
										<h4>
											<a href="single_new.html">Protecting Your Loved Ones with Life Insurance</a>
										</h4>
										<p><i className="fa fa-calendar" aria-hidden="true"></i> July 6, 2016</p>
										<p><i className="fa fa-user" aria-hidden="true"></i> By Admin</p>
									</div>
									<div className="tapo">
										<p>
											How much life insurance do you need? Your life insurance needs will depend
											on a number of factors, including the size of your family, the nature of
											your financial obligations, your career stage, and your...
										</p>
									</div>
									<Link to="#" className="ot-btn btn-sub-color">Read More</Link>
								</div>
							</div>
							{/* <!-- Group Disability Insurance --> */}
							<div className="item-new-list ">
								{/* <!-- add className no-position --> */}
								<div className="feature-new-warp">
									<Link to="#">
										<img src="./images/educational-concepts/10-Myths-Facts-About-Social-Security-Disability-Insurance.jpg"
											alt="10 Myths About Social Security"/>
									</Link>
								</div>
								<div className="box-new-info">
									<div className="new-info">
										<h4>
											<Link to="single_new.html">Group Disability Insurance</Link>
										</h4>
										<p><i className="fa fa-calendar" aria-hidden="true"></i> July 6, 2016</p>
										<p><i className="fa fa-user" aria-hidden="true"></i> By Admin</p>
									</div>
									<div className="tapo">
										<p>
											If a disabling illness or injury were to prevent you from working for weeks,
											months, or even years, how would you support yourself and your family? If
											disability strikes, you may discover that your most...
										</p>
									</div>
									<Link to="#" className="ot-btn btn-sub-color">Read More</Link>
								</div>
							</div>
							{/* <!-- Saving for Retirement --> */}
							<div className="item-new-list ">
								{/* <!-- add className no-position --> */}
								<div className="feature-new-warp">
									<Link to="#">
										<img src="./images/educational-concepts/retirement.png" alt="Retirement"/>
									</Link>
								</div>
								<div className="box-new-info">
									<div className="new-info">
										<h4>
											<a href="single_new.html">Saving for Retirement</a>
										</h4>
										<p><i className="fa fa-calendar" aria-hidden="true"></i> July 6, 2016</p>
										<p><i className="fa fa-user" aria-hidden="true"></i> By Admin</p>
									</div>
									<div className="tapo">
										<p>
											Although most of us recognize the importance of sound retirement planning,
											few of us embrace the nitty-gritty work involved. With thousands of
											investment possibilities, complex rules governing retirement plans, and so
											on, most people don't...
										</p>
									</div>
									<Link to="#" className="ot-btn btn-sub-color">Read More</Link>
								</div>
							</div>
							{/* <!-- Retirement Planning Key Numbers --> */}
							<div className="item-new-list ">
								{/* <!-- add className no-position --> */}
								<div className="feature-new-warp">
									<Link to="#">
										<img src="./images/educational-concepts/retirement-planning-401k.jpg"
											alt="Planning 401K"/>
									</Link>
								</div>
								<div className="box-new-info">
									<div className="new-info">
										<h4>
											<a href="single_new.html">Retirement Planning Key Numbers</a>
										</h4>
										<p><i className="fa fa-calendar" aria-hidden="true"></i> July 6, 2016</p>
										<p><i className="fa fa-user" aria-hidden="true"></i> By Admin</p>
									</div>
									<div className="tapo">
										<p>
											Retirement Planning Key Numbers Certain retirement plan and IRA limits are
											indexed for inflation each year, but only a few of the limits eligible for a
											cost-of-living adjustment (COLA) have increased for 2016. Some of...
										</p>
									</div>
									<Link to="#" className="ot-btn btn-sub-color">Read More</Link>
								</div>
							</div>
							{/* <!-- Investing Basics --> */}
							<div className="item-new-list ">
								{/* <!-- add className no-position --> */}
								<div className="feature-new-warp">
									<Link to="#">
										<img src="./images/educational-concepts/growing-team.jpg" alt="Growing Team"/>
									</Link>
								</div>
								<div className="box-new-info">
									<div className="new-info">
										<h4>
											<a href="single_new.html">Investing Basics</a>
										</h4>
										<p><i className="fa fa-calendar" aria-hidden="true"></i> July 6, 2016</p>
										<p><i className="fa fa-user" aria-hidden="true"></i> By Admin</p>
									</div>
									<div className="tapo">
										<p>
											Saving and Investing Wisely The first step in investing is to secure a
											strong financial foundation. More Details Building on Your Foundation Before
											you invest money, you should spend time setting your personal goals...
										</p>
									</div>
									<Link to="#" className="ot-btn btn-sub-color">Read More</Link>
								</div>
							</div>
							{/* <!-- Protecting Your Savings and Investments --> */}
							<div className="item-new-list ">
								{/* <!-- add className no-position --> */}
								<div className="feature-new-warp">
									<Link to="#">
										<img src="./images/educational-concepts/money-invest.png" alt="Protect Savings"/>
									</Link>
								</div>
								<div className="box-new-info">
									<div className="new-info">
										<h4>
											<a href="single_new.html">Protecting Your Savings and Investments</a>
										</h4>
										<p><i className="fa fa-calendar" aria-hidden="true"></i> July 6, 2016</p>
										<p><i className="fa fa-user" aria-hidden="true"></i> By Admin</p>
									</div>
									<div className="tapo">
										<p>
											In the wake of turbulence in the financial markets, it's worth reviewing the
											legal protections available for assets held by banks, credit unions, and
											securities dealers. Bank/savings and loan deposit accounts Generally,
											deposit accounts at...
										</p>
									</div>
									<Link to="#" className="ot-btn btn-sub-color">Read More</Link>
								</div>
							</div>
							{/* <!-- Estate Planning Key Numbers --> */}
							<div className="item-new-list ">
								{/* <!-- add className no-position --> */}
								<div className="feature-new-warp">
									<Link to="#">
										<img src="./images/educational-concepts/estate-planning.jpg" alt="Estate Planning"/>
									</Link>
								</div>
								<div className="box-new-info">
									<div className="new-info">
										<h4>
											<a href="single_new.html">Estate Planning Key Numbers</a>
										</h4>
										<p><i className="fa fa-calendar" aria-hidden="true"></i> July 6, 2016</p>
										<p><i className="fa fa-user" aria-hidden="true"></i> By Admin</p>
									</div>
									<div className="tapo">
										<p>
											You will find here some key numbers associated with estate planning, as well
											as the federal gift tax and estate tax rate schedules for 2015 and 2016.
											2015 2016 Annual gift tax exclusion: $14,000 $14,000...
										</p>
									</div>
									<Link to="#" className="ot-btn btn-sub-color">Read More</Link>
								</div>
							</div>
							{/* <!-- Bonds, Interest Rates, and the Impact of Inflation --> */}
							<div className="item-new-list ">
								{/* <!-- add className no-position --> */}
								<div className="feature-new-warp">
									<Link to="#">
										<img src="./images/educational-concepts/bonds-interest-rates.jpg" alt="Bonds, Interest Rates"/>
									</Link>
								</div>
								<div className="box-new-info">
									<div className="new-info">
										<h4>
											<a href="single_new.html">Bonds, Interest Rates, and the Impact of
												Inflation</a>
										</h4>
										<p><i className="fa fa-calendar" aria-hidden="true"></i> July 6, 2016</p>
										<p><i className="fa fa-user" aria-hidden="true"></i> By Admin</p>
									</div>
									<div className="tapo">
										<p>
											There are two fundamental ways that you can profit from owning bonds: from
											the interest that bonds pay, or from any increase in the bond's price. Many
											people who invest in bonds because they want...
										</p>
									</div>
									<Link to="#" className="ot-btn btn-sub-color">Read More</Link>
								</div>
							</div>
							{/* <!-- Saving for College --> */}
							<div className="item-new-list ">
								{/* <!-- add className no-position --> */}
								<div className="feature-new-warp">
									<Link to="#">
										<img src="./images/educational-concepts/spend-save.jpg" alt="Saving For College"/>
									</Link>
								</div>
								<div className="box-new-info">
									<div className="new-info">
										<h4>
											<a href="single_new.html">Saving for College</a>
										</h4>
										<p><i className="fa fa-calendar" aria-hidden="true"></i> July 6, 2016</p>
										<p><i className="fa fa-user" aria-hidden="true"></i> By Admin</p>
									</div>
									<div className="tapo">
										<p>
											There's no denying the benefits of a college education: the ability to
											compete in today's job market, increased earning power, and expanded
											horizons. But these advantages come at a price. And yet, year after year,...
										</p>
									</div>
									<Link to="#" className="ot-btn btn-sub-color">Read More</Link>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</section>
        </div>
    )
}


export default EducationalConcepts