import React from "react";
import { Link } from "react-router-dom";
const Overview = props => (
  <div className="iconbox-warp home-container-grid">
    <Link to="financial-planning" className="d-inline-block iconbox-link">
      <div className="text-center">
        <img
          src="/images/Home/icon_financial_planning.png"
          alt="Financial Markets"
          className="link-home-icon"
        />
      </div>
    </Link>
    <Link to="investment-management" className="d-inline-block iconbox-link">
      <div className="text-center">
        <img
          src="/images/Home/icon_investment_management.png"
          alt="Financial Markets"
          className="link-home-icon"
        />
      </div>
    </Link>
    <Link to="risk-management" className="d-inline-block iconbox-link">
      <div className="text-center">
        <img
          src="/images/Home/icon_risk_management.png"
          alt="Financial Markets"
          className="link-home-icon"
        />
      </div>
    </Link>
    <Link
      to="institutional-business-solutions"
      className="d-inline-block iconbox-link"
    >
      <div className="text-center">
        <img
          src="/images/Home/icon_institutional_business.png"
          alt="Financial Markets"
          className="link-home-icon"
        />
      </div>
    </Link>
    <Link to="financial-markets" className="d-inline-block iconbox-link">
      <div className="text-center">
        <img
          src="/images/Home/icon_financial_markets.png"
          alt="Financial Markets"
          className="link-home-icon"
        />
      </div>
    </Link>
    <Link to="economic-concepts" className="d-inline-block iconbox-link">
      <div className="text-center">
        <img
          src="/images/Home/icon_economic_concepts.png"
          alt="Economic Concepts"
          className="link-home-icon"
        />
      </div>
    </Link>
  </div>
);

export default Overview;
