import React from "react";

const Advantages = props => (
  <div>
    <p>Client Advantages</p>
    <ul>
      <li>
        Offer to Client the best tools for the well-being of your investments
      </li>
      <li>Access to the Technology</li>
      <li>Choose platform and services that fit clients needs</li>
      <li>Opportunity to control and Potentially Investment</li>
      <li>Access to a multitude of additional investment</li>
    </ul>
  </div>
);

export default Advantages;
