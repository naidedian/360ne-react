import React, { useState } from "react";
import { Link } from "react-router-dom";

const LetsTalk = props => {
  const [formData, setFormData] = useState({
    name: "",
    email: "",
    subject: "",
    comment: ""
  });

  const handleChange = e => {
    const { name, value } = e.target;
    setFormData({ ...formData, [name]: value });
  };

  return (
    <div>
      <section className="section-bg-wrapper bg-letstalk">
        <div className="sub-header-container">
          {/* <span>CONNECT WITH US</span> */}
          {/* <h3>LET'S TALK</h3> */}
          <ol className="breadcrumb">
            <li>
              <Link to="/">
                <i className="fa fa-home"></i> HOME
              </Link>
            </li>

            <li className="active">LET'S TALK</li>
          </ol>
        </div>
      </section>
      {/* <!-- /subheader --> */}

      <section>
        <div className="container">
          <div className="row">
            <div className="col-md-4">
              <div className="iconbox-inline">
                <span className="icon icon-location2"></span>
                <h4>Head Office</h4>
                <p>
                  4745 Ave. Isla Verde, Villas del Mar Este, CM 3,Carolina, PR
                  00979
                </p>
              </div>
            </div>
            <div className="col-md-4">
              <div className="iconbox-inline">
                <span className="icon icon-phone"></span>
                <h4>Phone Numbers</h4>
                <p>787-200-9124</p>
              </div>
            </div>
            <div className="col-md-4">
              <div className="iconbox-inline">
                <span className="icon icon-envelop"></span>
                <h4>E-mail Address</h4>
                <p>info@360rodriguezgroup.com</p>
              </div>
            </div>
          </div>
        </div>
      </section>
      {/* <!-- /Iconbxo --> */}
      {/* <div
        id="map-canvas"
        className="map-warp"
        style={{ height: "360px" }}
      ></div> */}
      {/* <!-- /Map --> */}

      <section>
        <div className="container">
          <div className="row">
            <div className="col-md-12">
              <div className="title-block title-contact">
                <h3>Send a Message</h3>
                <span className="bottom-title"></span>
              </div>
            </div>
            <form
              method="POST"
              onSubmit={e => {
                e.preventDefault();
                console.log(formData);
              }}
            >
              <div className="form-contact-warp">
                <div className="col-md-4">
                  <input
                    type="text"
                    className="form-control"
                    value={formData.name}
                    required="required"
                    title=""
                    placeholder="Full Name"
                    name="name"
                    onChange={e => handleChange(e)}
                  />
                </div>
                <div className="col-md-4">
                  <input
                    type="text"
                    className="form-control"
                    value={formData.email}
                    required="required"
                    title=""
                    placeholder="Email Address"
                    name="email"
                    onChange={e => handleChange(e)}
                  />
                </div>
                <div className="col-md-4">
                  <input
                    type="text"
                    className="form-control"
                    value={formData.subject}
                    required="required"
                    title=""
                    placeholder="Subject"
                    name="subject"
                    onChange={e => handleChange(e)}
                  />
                </div>
                <div className="col-md-12">
                  <div className="form-group">
                    <textarea
                      id="textarea"
                      className="form-control"
                      rows="5"
                      value={formData.comment}
                      required="required"
                      placeholder="Comment"
                      name="comment"
                      onChange={e => handleChange(e)}
                    ></textarea>
                  </div>
                </div>
                <div className="col-md-12">
                  <button type="submit" className="btn-main-color">
                    <i className="fa fa-paper-plane" aria-hidden="true"></i>{" "}
                    Submit
                  </button>
                </div>
              </div>
            </form>
          </div>
        </div>
      </section>
      {/* <!-- /Form Contact --> */}
    </div>
  );
};

export default LetsTalk;
